package 实现数据结构.exception;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:03 PM
 */
public class MyException extends RuntimeException {
    String msg;

    public MyException() {
        super();
    }

    public MyException(String msg) {
        super(msg);
        this.msg = msg;
    }
}
