package 实现数据结构.算法.链表;

/**
 * @DESC 定义节点
 * @Author tzq
 * @Date 2019-08-17 18:30
 **/
public class Node {
    public Node next;
    public int data;
    public Node(int data) {
        this.data = data;
    }
    public void display() {
        System.out.print(data + " ");
    }
}
