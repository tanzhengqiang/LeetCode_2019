package 实现数据结构.算法;

/**
 * @DESC https://leetcode-cn.com/problemset/algorithms/
 * @Author tzq
 * @Date 2019-08-17 17:56
 * <p>
 * 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
 * 示例 1:
 * <p>
 * 输入: 123
 * 输出: 321
 **/
public class _7_整数反转 {
    public static void main(String[] args) {
        System.out.println("reverse = " + mySolution(1534236469));
    }

    /**
     * 正确解法
     *
     * @param x
     * @return
     */
    public static int reverse2(int x) {
        long rev = 0;
        while (x != 0) {
            rev = rev * 10 + x % 10;
            x = x / 10;
        }
        if (rev > Integer.MAX_VALUE || rev < Integer.MAX_VALUE + 1) {
            return 0;
        }
        return (int) rev;
    }

    /**
     * 投机做法
     *
     * @param x
     * @return
     */
    public static int reverse(int x) {
        if (x > 0) {
            return Integer.parseInt(new StringBuilder().append(x).reverse().toString());
        } else {
            return Integer.parseInt(new StringBuilder().append(-x).reverse().toString()) * (-1);
        }
    }


    public static int mySolution(int x) {
        long tmp = 0;
        while (x != 0) {
            tmp = tmp * 10 + x % 10;
            x = x / 10;
        }
        if (tmp > Integer.MAX_VALUE || tmp < Integer.MIN_VALUE) {
            return 0;
        }
        return (int) tmp;
    }

}

