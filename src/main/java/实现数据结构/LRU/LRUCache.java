package 实现数据结构.LRU;

import java.util.LinkedHashMap;

/**
 * LRU缓存实现
 * <p>
 * 创建一个LinkedHashMap对象，并设置适当的初始容量。
 * 重写removeEldestEntry方法，根据需要定义 LRU 策略。例如，可以根据缓存的大小来删除最久未使用的条目。
 * 在添加或访问缓存项时，使用get或put方法进行操
 */
public class LRUCache {

    private LinkedHashMap<String, Object> cache;

    // 缓存大小
    private int cacheSize;

    public LRUCache(int cacheSize) {
        this.cache = new LinkedHashMap<>(16, 0.75f, true);
        this.cacheSize = cacheSize;
    }

    public Object get(String key) {
        // 如果键存在于缓存中，将其移到链表头部并返回值
        if (cache.containsKey(key)) {
            Object value = cache.get(key);
            cache.remove(key);
            cache.put(key, value);
            return value;
        }
        // 键不存在于缓存中，返回 null
        return null;
    }

    public void put(String key, Object value) {
        // 如果缓存已满，删除最久未使用的项
        if (cache.size() >= cacheSize) {
            cache.remove(cache.keySet().iterator().next());
        }
        // 添加新的键值对到缓存并将其移到链表头部
        cache.put(key, value);
    }

    public static void main(String[] args) {
        LRUCache cache = new LRUCache(2);

        cache.put("1", "A");
        cache.put("2", "B");
        System.out.println(cache.get("1"));  // 输出: A

        cache.put("3", "C");
        System.out.println(cache.get("2"));  // 输出: B

        cache.put("4", "D");
        System.out.println(cache.get("1"));  // 输出: null
        System.out.println(cache.get("3"));  // 输出: C
        System.out.println(cache.get("4"));  // 输出: D
    }
}