package 实现数据结构.实现动态数组;

/**
 * @DESC 存储key & value
 * @Author tzq
 * @Date 2019-10-06 17:30
 **/
public class Entry {

    int key;
    int value;
    Entry next;

    public Entry(int key, int value, Entry next) {
        this.key = key;
        this.value = value;
        this.next = next;
    }

}
