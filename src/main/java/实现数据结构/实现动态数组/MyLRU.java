package 实现数据结构.实现动态数组;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Title 实现LRU简单缓存
 * @Author zhengqiang.tan
 * @Date 2/18/23 1:49 PM
 */
public class MyLRU<K,V> extends LinkedHashMap<K,V> implements Map<K,V> {
    public MyLRU(int initialCapacity,float loadFactor,boolean accessOrder){
        super(initialCapacity,loadFactor,accessOrder);
    }

    @Override
    protected boolean removeEldestEntry(Entry<K, V> eldest) {
        if (size() > 6) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        MyLRU<Integer, String> lru = new MyLRU<Integer, String>(16,0.75f,true);
        for (int i = 0; i < 8; i++) {
            lru.put(i,"v-" +i);
        }

        System.out.println("lru = " + lru);
        System.out.println("lru.get(3) = " + lru.get(3));
        System.out.println("lru = " + lru);
        System.out.println("lru.size() = " + lru.size());
        lru.put(8,"test");
        System.out.println("lru = " + lru);


    }
}

//        lru = {2=v-2, 3=v-3, 4=v-4, 5=v-5, 6=v-6, 7=v-7}
//        lru.get(3) = v-3
//        lru = {2=v-2, 4=v-4, 5=v-5, 6=v-6, 7=v-7, 3=v-3}
//        lru.size() = 6

// 最多存8个元素，get(3)讲元素挪到了末尾，同时删除最近最少使用的0，1 始终保存6个元素。

