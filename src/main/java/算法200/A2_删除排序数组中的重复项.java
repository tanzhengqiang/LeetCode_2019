package 算法200;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 1/7/23 11:21 AM
 */
public class A2_删除排序数组中的重复项 {
    public static void main(String[] args) {
        int len = removeDuplicate(new int[]{0, 1, 2, 2, 3, 3, 4});
        System.out.println("len = " + len);
    }

    /**
     * 双指针法
     * @param nums
     * @return
     */
    public static int removeDuplicate(int[]nums){
        if (nums.length == 0) {
            return 0;
        }
        int i = 0;// 定义慢指针
        for (int j = 1; j < nums.length; j++) {
            if (nums[j] != nums[i]) {
                i++;
                nums[i] = nums[j];
            }

        }

        return i;
    }
}
