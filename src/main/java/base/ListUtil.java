package base;

import java.util.Random;

/**
 * @Description 链表工具类
 * @Created by zhengqiang
 * @Date 2023/1/7 15:18
 */
public class ListUtil {
    public static void main(String[] args) {
        System.out.println("new Random(10).nextInt() = " + new Random().nextInt(10));
        printList(generateListNodes());
    }

    //生成链表５个节点
    public static ListNode generateListNodes() {
        ListNode n5 = new ListNode(new Random().nextInt(10));
        ListNode n4 = new ListNode(new Random().nextInt(10), n5);
        ListNode n3 = new ListNode(new Random().nextInt(10), n4);
        ListNode n2 = new ListNode(new Random().nextInt(10), n3);
        ListNode n1 = new ListNode(new Random().nextInt(10), n2);
        printList(n1);
        return n1;
    }

    //打印链表
    public static void printList(ListNode node) {
        if (node == null) return;
        if (node.next != null) {
            System.out.print(node.val + "->");
        } else {
            System.out.println(node.val);
        }
        printList(node.next);
    }

}
