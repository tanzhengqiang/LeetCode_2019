package base;


/**
 * 树工具
 */
public class TreeUtil {

    public static TreeNode genRandomTree() {

        TreeNode left_l3 = new TreeNode(6);
        TreeNode right_l3 = new TreeNode(15);
        TreeNode left = new TreeNode(12, left_l3, right_l3);
        TreeNode right_l3_right = new TreeNode(30);
        TreeNode right = new TreeNode(25, null, right_l3_right);
        TreeNode root = new TreeNode(20, left, right);

        return root;
    }

//    public static TreeNode buildTree(List<Integer> list) {
//        if (list == null) return null;
//        Integer rootVal = list.remove(0);
//        TreeNode root = new TreeNode(rootVal);
//        root.left = buildTree(list);
//        root.right = buildTree(list);
//        return root;
//    }


    public static void main(String[] args) {
        printTree(genRandomTree());
    }

    public static void printTree(TreeNode root) {
        if (root == null) return;
        System.out.print(root.val + " ");
        printTree(root.left);
        printTree(root.right);

    }
}
