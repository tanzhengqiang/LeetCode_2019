package base;

/**
 * @Description 链表节点
 * @Created by zhengqiang
 * @Date 2023/1/7 14:26
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
