package base;

/**
 * @Description 数组工具
 * @Created by zhengqiang
 * @Date 2023/1/7 15:14
 */
public class ArrayUtil {
    public static void main(String[] args) {
        System.out.println("printArray(new int[]{3,2,3,5}) = " + printArray(new int[]{3, 2, 3, 5}));
    }

    public static String printArray(int[] arr) {
        StringBuilder builder = new StringBuilder("[");
        for (int i = 0; i < arr.length; i++) {
            builder.append(arr[i]);
            if ((i + 1) == arr.length) {
                builder.append("]");
            } else {
                builder.append(",");
            }
        }
        return builder.toString();
    }
}
