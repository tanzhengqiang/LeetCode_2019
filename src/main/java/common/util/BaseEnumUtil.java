package common.util;

import Java基础.enums.BaseEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

public class BaseEnumUtil {

    /**
     * 根据枚举码获取枚举对象
     *
     * @param code
     * @param clz
     * @param <T>
     * @return
     */
    public static <T extends BaseEnum> T getByCode(String code, Class<T> clz) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (T e : clz.getEnumConstants()) {
            if (StringUtils.equals(e.code(), code)) {
                return e;
            }
        }
        return null;
    }

    /**
     * 根据枚举码获取枚举对象Map
     *
     * @param <T>
     * @return
     */
    public static <T extends BaseEnum> Map<String, String> getEnumMapByClass(Class<T> clz) {
        Map<String, String> enumMap = new LinkedHashMap<>();
        for (T e : clz.getEnumConstants()) {
            enumMap.put(e.code(), e.message());
        }
        return enumMap;
    }

    /**
     * 获取枚举码
     * 枚举对象为空时，返回null
     *
     * @param BaseEnum
     * @return
     */
    public static String getCodeNullIfEmpty(BaseEnum BaseEnum) {
        return getCodeDefaultIfEmpty(BaseEnum, null);
    }

    /**
     * 获取枚举码
     * 枚举对象为空时，返回空串
     *
     * @param BaseEnum
     * @return
     */
    public static String getCodeDefaultIfEmpty(BaseEnum BaseEnum) {
        return getCodeDefaultIfEmpty(BaseEnum, StringUtils.EMPTY);
    }

    /**
     * 获取枚举码
     * 枚举对象为空时，返回默认值
     *
     * @param BaseEnum
     * @param defaultStr
     * @return
     */
    public static String getCodeDefaultIfEmpty(BaseEnum BaseEnum, String defaultStr) {
        return null == BaseEnum ? defaultStr : BaseEnum.code();
    }
}
