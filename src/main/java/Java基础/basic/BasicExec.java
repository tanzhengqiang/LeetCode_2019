package Java基础.basic;

/**
 * @Title i++/++i的理解
 * i++ 即后加加，原理是：先自增，然后返回自增之前的值
 * ++i 即前加加，原理是：先自增，然后返回自增之后的值
 * 重点：这是一般人所不知道的，记住：不论是前++还是后++，都有个共同点是先自增。
 * 对于++i 就不说了，大多数人都懂，而对于 i++ 的原理，我用代码模拟其原理，如下：
 * @Author zhengqiang.tan
 * @Date 2021/6/20 11:47 AM
 */
public class BasicExec {

    public static void main(String[] args) {

        int a = 0;
        for (int i = 0; i < 99; i++) {
            a = a++;//a 自增后，ia= 1，但是接着返回自增之前的值0，此时表达式变成 a= 0
        }
        System.out.println(a); // 0


        int a1 = 2;
        int b1 = (3 * a1++) + a1; // (3*2)+3
        System.out.println(b1);   // 结果：9


        int b = 0;
        for (int i = 0; i < 99; i++) {
            b = ++b;
        }
        System.out.println(b); // 99

        Integer c1 = 0;
        int c2 = 0;
        for (int i = 0; i < 99; i++) {
            c1 = c1++;
            c2 = c1++;
        }
        System.out.println("c1=" + c1 + " c2=" + c2);// 99 98


        int i = 1;
        int j = 1;
        int k = i++ + ++i + ++j + j++; // 1+3 + 2+2
        System.out.println(k); // 8


    }


}
