package Java基础.jol;

import org.openjdk.jol.info.ClassLayout;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/3/28 5:19 PM
 */
public class TestObj {
    public static void main(String[] args) {
        Object obj = new Object();
        System.out.println(ClassLayout.parseClass(Object.class).toPrintable(obj));


    }
}
