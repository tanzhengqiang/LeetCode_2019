package Java基础.jol;

import org.openjdk.jol.info.ClassLayout;

/**
 * @Title https://www.jianshu.com/p/6d62c3ee48d0
 * @Author zhengqiang.tan
 * @Date 2021/2/25 11:06 AM
 */
public class Demo2 {

    public static class A {
    }

    public static class B {
        private long s;
    }

    public  static  class C {
        private int a;
        private long s;
    }


    public static void main(String[] args) {


        A a = new A();
        System.out.println(ClassLayout.parseClass(A.class).toPrintable(a));
        B b = new B();
        System.out.println(ClassLayout.parseClass(B.class).toPrintable(b));
        C c = new C();
        System.out.println(ClassLayout.parseClass(C.class).toPrintable(c));
        int[] aa = new int[10];
        aa.clone();

        System.out.println(ClassLayout.parseClass(int.class).toPrintable(aa));

    }
}
