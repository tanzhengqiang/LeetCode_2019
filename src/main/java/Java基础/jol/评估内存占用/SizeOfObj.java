package Java基础.jol.评估内存占用;

import com.carrotsearch.sizeof.RamUsageEstimator;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2/19/23 2:53 PM
 */
public class SizeOfObj {
    public static void main(String[] args) {
        String str = "abcd";
        System.out.println("RamUsageEstimator.sizeOf(str) = " + RamUsageEstimator.sizeOf(str)); // 48个字节

        List<Integer> list = new ArrayList<>();
        System.out.println("RamUsageEstimator.sizeOf(list) = " + RamUsageEstimator.sizeOf(list)); // 40个字节


        Boolean bol = null;
        System.out.println("RamUsageEstimator.sizeOf(bol) = " + RamUsageEstimator.sizeOf(bol)); // 16个字节
        boolean bol2 = true;
        System.out.println("RamUsageEstimator.sizeOf(bol2) = " + RamUsageEstimator.sizeOf(bol2)); // 16个字节


    }
}
