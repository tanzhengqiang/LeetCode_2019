package Java基础.jol;

import org.openjdk.jol.info.ClassLayout;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/2/25 10:27 AM
 * 参考： https://zhuanlan.zhihu.com/p/151856103?from_voters_page=true
 *
 */
public class JolDemo {
    public static void main(String[] args) {
//        Object o = new Object();
//        System.out.println(ClassLayout.parseClass(Object.class).toPrintable(o));
//
//
//        synchronized (o) {
//            System.out.println(ClassLayout.parseClass(Object.class).toPrintable(o));
//        }

        T t = new T();
        System.out.println(ClassLayout.parseClass(T.class).toPrintable(t));



    }

    static class T {
        int num = 10;
        String s = "hello";
    }
}


//    javacore.jol.JolDemo.T object internals:
//        OFFSET  SIZE   TYPE DESCRIPTION                    VALUE
//        0     4        (object header)                01 00 00 00 (0000 0001 0000 0000 0000 0000 0000 0000)
//        4     4        (object header)                00 00 00 00 (0000 0000 0000 0000 0000 0000 0000 0000)
//        8     4        (object header)                43 c1 00 f8 (0100 0011 1100 0001 0000 0000 1111 1000)
//        12     4    int T.num                          10
//        16     4 String T.s                            (object)
//        20     4        (loss due to the next object alignment)
//        Instance size: 24 bytes (estimated, add this JAR via -javaagent: to get accurate result)
//        Space losses: 0 bytes internal + 4 bytes external = 4 bytes total