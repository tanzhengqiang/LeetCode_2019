package Java基础.JUC同步工具.AQS;

import java.util.concurrent.locks.Lock;

/**
 * 测试自定义的锁 使用 CustomReentrantLock
 */
public class Demo {
    private static final Lock lock = new CustomReentrantLock(true); // 使用公平锁

    public static void main(String[] args) {
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Thread 1 acquired lock");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Thread 2 acquired lock");
            } finally {
                lock.unlock();
            }
        }).start();
    }
}