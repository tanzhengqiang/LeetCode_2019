package Java基础.JUC同步工具.Semaphore;

import java.util.concurrent.Semaphore;

/**
 * @Title 信号量：Semaphore的本质就是协调多个线程对共享资源的获取
 * Semaphore可以控同时访问的线程个数，通过 acquire() 获取一个许可，如果没有就等待，而 release() 释放一个许可。
 *
 * 用途：它可以用于做流量控制，特别是公用资源有限的应用场景，比如数据库连接
 * <p>
 * 假若一个工厂有5台机器，但是有8个工人，一台机器同时只能被一个工人使用，只有使用完了，其他工人才能继续使用。
 * <p>
 * 那么我们就可以通过Semaphore来实现。
 * @Author zhengqiang.tan
 * @Date 2021/4/12 8:53 PM
 */
public class Demo2 {
    public static void main(String[] args) {
        int N = 8;            //工人数
        Semaphore semaphore = new Semaphore(5); //机器数目
        for (int i = 0; i < N; i++)
            new Worker(i, semaphore).start();
    }

    // 工人
    static class Worker extends Thread {
        private int num;
        private Semaphore semaphore;

        public Worker(int num, Semaphore semaphore) {
            this.num = num;
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                System.out.println("工人" + this.num + "占用一个机器在生产...");
                Thread.sleep(2000);
                System.out.println("工人" + this.num + "释放出机器");
                semaphore.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
