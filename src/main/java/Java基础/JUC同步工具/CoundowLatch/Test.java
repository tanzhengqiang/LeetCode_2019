package Java基础.JUC同步工具.CoundowLatch;

import java.util.concurrent.CountDownLatch;

/**
 * @Title CountDownLatch类位于java.util.concurrent包下，利用它可以实现类似计数器的功能。
 * 比如有一个任务A，它要等待其他2个任务执行完毕之后才能执行，此时就可以利用CountDownLatch来实现这种功能。
 * @Author zhengqiang.tan
 * @Date 2021/4/12 8:48 PM
 */
public class Test {
    public static void main(String[] args) {
        final CountDownLatch latch = new CountDownLatch(2); // 计数
        new Thread(() -> {
            try {
                System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
                Thread.sleep(3000);
                System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"A Thread").start();

        new Thread(() -> {
            try {
                System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
                Thread.sleep(3000);
                System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
                latch.countDown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"B Thread").start();

        try {
            System.out.println("等待2个子线程执行完毕...");
            latch.await();
            System.out.println("2个子线程已经执行完毕");
            System.out.println("继续执行主线程....");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
