package Java基础.JUC同步工具.CyclicBarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Title 同步屏障
 * <p>
 * CountDownLatch的使用是一次性的，无法重复利用，用CyclicBarrier就可以实现多次循环，因为它可以重复利用
 * <p>
 * <p>
 * CyclicBarrier和CountDownLatch有什么区别？
 * 两者最核心的区别：
 * CountDownLatch是一次性的，而CyclicBarrier则可以多次设置屏障，实现重复利用；
 * CountDownLatch中的各个子线程不可以等待其他线程，只能完成自己的任务；而CyclicBarrier中的各个线程可以等待其他线程
 * @Author zhengqiang.tan
 * @Date 2021/6/19 10:36 AM
 */
public class CyclicBarrierTest {
    public static void main(String[] args) {
        // 参与协作的线程数, 一次屏障后需要执行的动作
        CyclicBarrier barrier = new CyclicBarrier(20, () -> System.out.println("满20人发车"));

        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}

// 执行5次