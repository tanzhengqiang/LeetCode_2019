package Java基础.JUC同步工具.Queue;

import java.util.concurrent.LinkedTransferQueue;

/**
 * @Title 交替打印
 * @Author zhengqiang.tan
 * @Date 2021/3/28 10:51 AM
 */
public class T01_TransferQueue {
    public static void main(String[] args) {
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();
        LinkedTransferQueue<Character> queue = new LinkedTransferQueue<>();

        new Thread(() -> {
            for (char c : aI) {
                try {
                    System.out.println(queue.take());// 上来就从队列拿值，没有就等着
                    queue.transfer(c); // 队列中再添加 1
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1").start();
        new Thread(() -> {
            for (char c : aC) {
                try {
                    // 当生产者利用transfer()方法发送消息给消费者时，生产者将一直被阻塞，直到消息被使用为止。
                    queue.transfer(c); // 上来就是往队列里面添加A
                    System.out.println(queue.take()); // 取 1
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t2").start();
    }
}
