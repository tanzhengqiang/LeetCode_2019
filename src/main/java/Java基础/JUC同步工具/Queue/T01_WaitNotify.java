package Java基础.JUC同步工具.Queue;


/**
 * @Title 交替打印
 * @Author zhengqiang.tan
 * @Date 2021/3/28 10:51 AM
 * wait() 语义：使得当前线程立刻停止运行，处于等待状态（WAIT），并将当前线程置入锁对象的等待队列中，直到被通知（notify）或被中断为止。
 * notify（）语义：唤醒处于等待状态的线程
 * notify唤醒是按照wait的顺序的，wait后的线程会进入一个FIFO队列中，notify是一个有序的出队列的过程
 */
public class T01_WaitNotify {

    public static void main(String[] args) {
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();

        final Object o = new Object();


        new Thread(() -> {
            synchronized (o) {
                for (char c : aI) {
                    System.out.print(c); // 先输出
                    try {
                        o.notify(); // 叫醒t2
                        o.wait(); // 让出锁 自己进行wait等待
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    o.notify(); // 必须要有，否则无法停止程序了
                }
            }
        }, "t1").start();

        new Thread(() -> {
            synchronized (o) {
                for (char c : aC) {
                    System.out.print(c);
                    try {
                        o.notify();
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    o.notify();
                }
            }
        }, "t2").start();


    }
}
