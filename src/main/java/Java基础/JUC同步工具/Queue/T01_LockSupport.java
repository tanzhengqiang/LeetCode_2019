package Java基础.JUC同步工具.Queue;

import java.util.concurrent.locks.LockSupport;

/**
 * @Title 交替打印
 * @Author zhengqiang.tan
 * @Date 2021/3/28 10:51 AM
 */
public class T01_LockSupport {
    static Thread t1 = null, t2 = null;

    public static void main(String[] args) {
        char[] aI = "123456".toCharArray();
        char[] aC = "ABCDEF".toCharArray();

        t1 = new Thread(() -> {
            for (char c : aC) {
                System.out.println(c);
                LockSupport.unpark(t2); // t1叫醒t2
                LockSupport.park(); // t1 阻塞
            }
        }, "t1");
        t2 = new Thread(() -> {
            for (char c : aI) {
                LockSupport.park(); // t2阻塞
                System.out.println(c);
                LockSupport.unpark(t1); // t2 叫醒t1
            }
        }, "t2");

        t2.start();
        t1.start();


    }
}
