package Java基础.泛型;

/**
 * @Title 案例一：演示泛型
 * @Author zhengqiang.tan
 * @Date 2021/2/1 11:21 AM
 */
public class GlmapperGeneric<T> {
    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    /**
     * 不指定类型
     */
    public void noSpecifyType() {
        GlmapperGeneric glmapperGeneric = new GlmapperGeneric();
        glmapperGeneric.set("test");
        // 需要强制类型转换
        String test = (String) glmapperGeneric.get();
        System.out.println(test);
    }

    /**
     * 指定类型
     * 这段代码中的 specifyType 方法中 省去了强制转换，可以在编译时候检查类型安全，可以用在类，方法，接口上。
     */
    public void specifyType() {
        GlmapperGeneric<String> glmapperGeneric = new GlmapperGeneric();
        glmapperGeneric.set("test");
        // 不需要强制类型转换
        String test = glmapperGeneric.get();
        System.out.println(test);
    }

    public static void main(String[] args) {
        new GlmapperGeneric<String>().specifyType();
        new GlmapperGeneric().noSpecifyType();
    }
}