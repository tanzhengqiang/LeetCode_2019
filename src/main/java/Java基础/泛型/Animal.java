package Java基础.泛型;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title 案例二： 演示泛型上届
 * @Author zhengqiang.tan
 * @Date 2021/2/1 11:28 AM
 * https://mp.weixin.qq.com/s/0AZY4XFO6AOyuihshKYtzQ
 * <p>
 * 对于不确定或者不关心实际要操作的类型，可以使用无限制通配符（尖括号里一个问号，即 <?> ），
 * 表示可以持有任何类型。像 countLegs 方法中，限定了上届，但是不关心具体类型是什么，
 * 所以对于传入的 Animal 的所有子类都可以支持，并且不会报错。而 countLegs1 就不行。
 */
public abstract class Animal {
    abstract void shout();
    abstract int countLegs();

    public static void main(String[] args) {
        List<Dog> dogs = new ArrayList<>();
        // 不会报错
        countLegs(dogs);
        // 报错: 这里接受的是Animal 但是传过来的是Dog
//        countLegs1(dogs);
    }

    static int countLegs(List<? extends Animal> animals) {
        int retVal = 0;
        for (Animal animal : animals) {
            retVal += animal.countLegs();
        }
        return retVal;
    }

    static int countLegs1(List<Animal> animals) {
        int retVal = 0;
        for (Animal animal : animals) {
            retVal += animal.countLegs();
        }
        return retVal;
    }

}

class Cat extends Animal {
    @Override
    void shout() {
        System.out.println("Cat: miao miao ....");
    }

    @Override
    int countLegs() {
        return 4;
    }
}

class Dog extends Animal {
    @Override
    void shout() {
        System.out.println("Dog: wang wang ....");
    }

    @Override
    int countLegs() {
        return 4;
    }
}