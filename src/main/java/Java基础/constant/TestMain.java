package Java基础.constant;


/**
 * @Author zhengqiang.tan
 * @Date 2020/9/8 10:41 AM
 * @Version 1.0
 * <p>
 * ++在前表示先进行++操作，再进行赋值；
 */
public class TestMain {
    public static void main(String[] args) {
        int a = 3;
        int b = --a; //a = a -1;int b = a;
        int c = a++; //int c = a;a = a + 1;
        System.out.println("b=" + b + " c=" + c); //b=2 c=2

    }
}
