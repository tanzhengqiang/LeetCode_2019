package Java基础.constant;

/**
 * @Title 静态代码块 非静态代码块  构造方法执行顺序
 * @Author zhengqiang.tan
 * @Date 2021/4/12 8:56 PM
 */
public class TestStatic {
    public static void main(String[] args) {
        // System.out.println(B.num);
//        A的静态代码块
//        B的静态代码块
//        10

        new B();

        // 总结：
        /**
         * 1、new 子类的时候先调用父类的静态代码块，再调用子类的静态代码块
         * 2、再调用父的非静态代码块、父的无参构造器
         * 3、在调用子的非静态代码块、子的无参构造器
         *
         *
         *         A的静态代码块 B的静态代码块
         *         A 的非静态代码块  A的无参构造器
         *         B 的非静态代码块  B 的无参构造器
         * */


    }
}


class A {
    static int num = 100;
    int min = 1;

    {
        System.out.println("A 的非静态代码块"); // 随对象存在，创建几个对象就执行几次 且优先于构造方法执行
    }

    static {
        System.out.println("A的静态代码块 "); // 随类存在，仅在类初始化时执行一次
//        int aa = min; 不可以访问非静态成原变量，初始化顺序仅仅是次要原因，根本原因是此时的成员还没有随对象被创建出来

        System.out.println(num);
    }

    A() {
        System.out.println("A的无参构造器");
    }
}

class B extends A {

    {
        System.out.println("B 的非静态代码块");
    }

    static {
        System.out.println("B的静态代码块 ");
    }

    B() {
        System.out.println("B 的无参构造器");
    }

    static int num = 10;

}
