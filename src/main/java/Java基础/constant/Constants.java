package Java基础.constant;

/**
 * 常量定义最佳实践
 * <p>
 * 创建一个构造方法私有的final类存放常量，需要的类import引用以后调用常量。
 * 不必重复定义常量的同时，保证这个类的功能单一不会被实例化也不会被继承。
 *
 * @Author zhengqiang.tan
 * @Date 2020/9/8 10:40 AM
 * @Version 1.0
 */
public final class Constants {
    private Constants() {
    }
    public static final String COMMON_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
    public static final int TRUE = 1;
    public static final int FALSE = 0;
}
