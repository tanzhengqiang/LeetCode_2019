package Java基础.Concurrent;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author zhengqiang.tan
 * @Date 2020/10/15 2:09 PM
 * @Version 1.0
 *
 *
 * latch 的数量加上 map 的 size，总数应该是 20，但运行之后，大概率不是，我们丢失了部分数据。原因就是，main 方法里使用了 HashMap 类，它并不是线程安全的，在并发执行时发生了错乱，造成了错误的结果，将 HashMap 换成 ConcurrentHashMap 即可解决问题。
 *
 *
 */
public class CountDownLatchTest {
    public static void main(String[] args) {
        final String userid = "123";
//        final SlowInterfaceMock mock = new SlowInterfaceMock();
        ParallelFetcher fetcher = new ParallelFetcher(20, 50);
        final ConcurrentHashMap<String, String> result = new ConcurrentHashMap<>();

//        fetcher.submitJob(() -> result.put("method0", mock.method0(userid)));
//        fetcher.submitJob(() -> result.put("method1", mock.method1(userid)));
//        fetcher.submitJob(() -> result.put("method2", mock.method2(userid)));

        fetcher.await();
        System.out.println(fetcher.latch);
        System.out.println(result.size());
        fetcher.dispose();
    }
}
