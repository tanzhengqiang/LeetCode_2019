package Java基础.enums;

/**
 * @Author zhengqiang.tan
 * @Date 2020/9/4 3:51 PM
 * @Version 1.0
 */
public enum ApplyStatusTypeEnum implements BaseEnum {

    PASS("PASS", "通过"),
    DENY("DENY", "已拒绝"),
    ACTIVE("ACTIVE", "待审批"),
    ALL("ALL", "全部");


    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    private String code;
    private String message;

    ApplyStatusTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
