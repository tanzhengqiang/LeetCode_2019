package Java基础.enums;

/**
 * @Author zhengqiang.tan
 * @Date 2020/9/4 3:47 PM
 * @Version 1.0
 */
public enum EnvTypeEnum implements BaseEnum {
    PROD("PROD", "生产环境"),
    TEST("TEST", "测试环境");

    private String code;
    private String message;

    EnvTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


    @Override
    public String code() {
        return this.code;
    }

    @Override
    public String message() {
        return this.message;
    }

}
