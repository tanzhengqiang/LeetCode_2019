package Java基础.enums;

import common.util.BaseEnumUtil;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author zhengqiang.tan
 * @Date 2020/9/4 3:53 PM
 * @Version 1.0
 */
public class EnumTest {
    public static void main(String[] args) {
        Map<String, Map<String, String>> mapResult = new EnumTest().enumList();
        mapResult.entrySet().stream().forEach(m -> System.out.println(m.getKey() + "--" + m.getValue().keySet().toString()));
//        EnvTypeEnum--[PROD, TEST]
//        ApplyStatusTypeEnum--[PASS, DENY, ACTIVE, ALL]

    }

    public Map<String, Map<String, String>> enumList() {
        try {
            // 指定所在包下，获取所有枚举
            Reflections reflections = new Reflections("javacore.enums");
            //获取所有实现EnumBase的枚举
            Map<String, Map<String, String>> mapList = new HashMap<>();
            Set<Class<? extends BaseEnum>> classSet = reflections.getSubTypesOf(BaseEnum.class);
            for (Class cl : classSet) {
                mapList.put(cl.getSimpleName(), BaseEnumUtil.getEnumMapByClass(cl));
            }
            // 单独加一个库的枚举
//            mapList.put(DbTypeEnum.class.getSimpleName(), EnumBaseUtil.getEnumMapByClass(DbTypeEnum.class));
            return mapList;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
