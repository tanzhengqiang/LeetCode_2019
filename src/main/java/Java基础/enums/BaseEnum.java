package Java基础.enums;

/**
 * @Author zhengqiang.tan
 * @Date 2020/9/4 3:44 PM
 * @Version 1.0
 */
public interface BaseEnum {
    String code();
    String message();
}
