package Java基础.annotation;


import java.lang.reflect.Method;

/**
 * 注解处理器
 */
public class IntroduceHandler {
    public void handle() {
        Class<Person> myClass = Person.class;

        Method[] declaredMethods = myClass.getDeclaredMethods();

        for (Method method : declaredMethods) {
            if (method.isAnnotationPresent(Introduce.class)) {
                Introduce introduce = method.getAnnotation(Introduce.class);
                String name = introduce.name();
                int age = introduce.age();
                String address = introduce.address();
                System.out.println("name:" + name + ",age:" + age + ",address:" + address);
            }
        }

    }

}
