package Java基础.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 可以考虑：
 * 将用户权限用枚举的方式给出，注解元素表明某个方法必须拥有某些权限才能调用，拦截器拦截请求方法，
 * 用户是否有权限对该方法进行调用，根据用户不同的权限进行不同的处理。
 *
 * @Author zhengqiang.tan
 * @Date 2020/9/7 4:43 PM
 * @Version 1.0
 * https://www.cnblogs.com/pepcod/archive/2013/02/16/2913474.html
 */
public class TestMain {
    public static void main(String[] args) {
        List<Integer> useCases = new ArrayList<>();
        Collections.addAll(useCases, 47, 48, 49, 50);
        trackUseCases(useCases, TestMain.class);
    }


    /**
     * 从原理上讲，注解处理器就是通过反射机制获取被检查方法上的注解信息，然后根据注解元素的值进行特定的处理。
     *
     * @param useCases
     * @param cl
     */
    public static void trackUseCases(List<Integer> useCases, Class<?> cl) {
        for (Method m : cl.getDeclaredMethods()) {
            UseCase uc = m.getAnnotation(UseCase.class);
            if (uc != null) {
                System.out.println("Found Use Case:" + uc.id() + " " + uc.description());
                useCases.remove(new Integer(uc.id()));
            }
        }
        for (int i : useCases) {
            System.out.println("Warning: Missing use case-" + i);
        }
    }


    @UseCase(id = 47, description = "Passwords must contain at least one numeric")
    public boolean validatePassword(String password) {
        return (password.matches("\\w*\\d\\w*"));
    }

    @UseCase(id = 48)
    public String encryptPassword(String password) {
        return new StringBuilder(password).reverse().toString();
    }


}
