package Java基础.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 定义注解
 */
@Target(ElementType.METHOD) // 作用在方法上
@Retention(RetentionPolicy.RUNTIME)  // 运行时可见（默认编译时可见）
public @interface Introduce {

    String name() default "";

    int age() default 0;

    String address() default "";

}
