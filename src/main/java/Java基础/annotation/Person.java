package Java基础.annotation;

/**
 * @Description TODO
 * @Author zhengqiang.tan
 * @Date 2024/6/2 09:27
 */
public class Person {

    public static void main(String[] args) {
        new IntroduceHandler().handle();
        new Person().sayHello("xiaoming");
    }

    @Introduce(name = "Tan", age = 18, address = "shanghai")
    public void sayHello(String name) {
        System.out.println("Hello " + name);
    }
}

