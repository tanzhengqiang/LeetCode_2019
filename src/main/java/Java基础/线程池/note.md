#### 线程池核心要点
[参考资料地址](https://github.com/chenjiabing666/JavaFamily/blob/master/docs/sanfene/javathread.md)
- 作用

  1. 它帮我们管理线程，避免增加创建线程和销毁线程的资源损耗。因为线程其实也是一个对象，创建一个对象，需要经过类加载过程，销毁一个对象，需要走GC垃圾回收流程，都是需要资源开销的。
  2. 提高响应速度： 如果任务到达了，相对于从线程池拿线程，重新去创建一条线程执行，速度肯定慢很多。
  3. 重复利用： 线程用完，再放回池子，可以达到重复利用的效果，节省资源。
  
<br/>
  
- 核心参数  
    线程池有七大参数，需要重点关注corePoolSize、maximumPoolSize、workQueue、handler这四个。

    - corePoolSize  
    此值是用来初始化线程池中核心线程数，当线程池中线程池数< corePoolSize时，系统默认是添加一个任务才创建一个线程池。当线程数 = corePoolSize时，新任务会追加到workQueue中。
    
    - maximumPoolSize  
    maximumPoolSize表示允许的最大线程数 = (非核心线程数+核心线程数)，当BlockingQueue也满了，但线程池中总线程数 < maximumPoolSize时候就会再次创建新的线程。

    - keepAliveTime  
    非核心线程 =(maximumPoolSize - corePoolSize ) ,非核心线程闲置下来不干活最多存活时间。
    
    - unit  
    线程池中非核心线程保持存活的时间的单位
    TimeUnit.DAYS; 天  
    TimeUnit.HOURS; 小时  
    TimeUnit.MINUTES; 分钟  
    TimeUnit.SECONDS; 秒  
    TimeUnit.MILLISECONDS; 毫秒  
    TimeUnit.MICROSECONDS; 微秒  
    TimeUnit.NANOSECONDS; 纳秒  

    - workQueue  
    线程池等待队列，维护着等待执行的Runnable对象。当运行当线程数= corePoolSize时，新的任务会被添加到workQueue中，如果workQueue也满了则尝试用非核心线程执行任务，等待队列应该尽量用有界的。
    
    - threadFactory  
    创建一个新线程时使用的工厂，可以用来设定线程名、是否为daemon线程等等。
    
    - handler  
    corePoolSize、workQueue、maximumPoolSize都不可用的时候执行的饱和策略。
         > AbortPolicy ：直接抛出异常，默认使用此策略  
      CallerRunsPolicy：用调用者所在的线程来执行任务  
      DiscardOldestPolicy：丢弃阻塞队列里最老的任务，也就是队列里靠前的任务  
      DiscardPolicy ：当前任务直接丢弃  
      想实现自己的拒绝策略，实现RejectedExecutionHandler接口即可。


- 工作中的应用  
  数据推送，数据清洗啊、数据统计


- 线程池关闭  
  shutdown 和shutdownnow简单来说区别如下：

  shutdownNow()能立即停止线程池，正在跑的和正在等待的任务都停下了。这样做立即生效，但是风险也比较大。
  shutdown()只是关闭了提交通道，用submit()是无效的；而内部的任务该怎么跑还是怎么跑，跑完再彻底停止线程池。

<br/>

-  线程池的线程数应该怎么配置？  

    线程在Java中属于稀缺资源，线程池不是越大越好也不是越小越好。任务分为计算密集型、IO密集型、混合型。
    
    计算密集型：大部分都在用CPU跟内存，加密，逻辑操作业务处理等。  
    IO密集型：数据库链接，网络通讯传输等。
   
    > 一般的经验，不同类型线程池的参数配置：  
    - 计算密集型: 一般推荐线程池不要过大，一般是CPU数 + 1，+1是因为可能存在页缺失(就是可能存在有些数据在硬盘中需要多来一个线程将数据读入内存)。如果线程池数太大，可能会频繁的 进行线程上下文切换跟任务调度。获得当前CPU核心数代码如下：
  `Runtime.getRuntime().availableProcessors(); `   
    - IO密集型：线程数适当大一点，机器的Cpu核心数*2。  
    - 混合型：可以考虑根绝情况将它拆分成CPU密集型和IO密集型任务，如果执行时间相差不大，拆分可以提升吞吐量，反之没有必要。  
     当然，实际应用中没有固定的公式，需要结合测试和监控来进行调整。


- 常见的处理线程池异常的方法：  
  使用 try-catch 块捕获异常。  
  在任务执行前后添加钩子函数：通过实现 ThreadPoolExecutor 类中的 beforeExecute 和 afterExecute 方法，在任务执行的前后添加一些自定义的操作，如记录日志、统计线程执行时间等。  
  使用 UncaughtExceptionHandler 处理未被捕获的异常：在线程抛出异常且没有被及时捕获或者处理的情况下，可以设置 Thread.UncaughtExceptionHandler 来处理未捕获的异常。


