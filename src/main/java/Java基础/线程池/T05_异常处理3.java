package Java基础.线程池;

import java.util.concurrent.*;

/**
 * 异常处理：重写afterExecute ，然后执行 submit提交
 */
public class T05_异常处理3 {

    public static void main(String[] args) {

        //1.创建一个自己定义的线程池,重写afterExecute方法
        ExecutorService service = new ThreadPoolExecutor(1, 1,
                0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue(10)) {
            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                super.afterExecute(r, t);
                if (t != null) {
                    System.out.println("afterExecute里面获取到异常信息" + t.getMessage());
                }

                //如果r的实际类型是FutureTask 那么是submit提交的，所以可以在里面get到异常
                if (r instanceof FutureTask) {
                    try {
                        Future<?> future = (Future<?>) r;
                        future.get();
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                }
            }
        };


        //2.提交任务
        service.submit(() ->
        {
            int i = 1 / 0;
        });

        service.shutdownNow();

    }
}



