package Java基础.线程池;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @Title 定时任务
 * @Author zhengqiang.tan
 * @Date 2021/4/23 10:29 PM
 */
public class T04_ScheduledExecutorService {

    public static void main(String[] args) throws Exception{
        //创建一个定长线程池
        ScheduledExecutorService sched = Executors.newScheduledThreadPool(5);
        //参数的释义     		线程任务 、 延迟时间 、 间隔时间 、 时间单位
        sched.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {
                System.out.println("当前线程的名字："+Thread.currentThread().getName());
            }
        }, 1, 2, TimeUnit.SECONDS);// 内部 Runnable类     延迟1s 每次间隔2s  时间单位秒

        //主线程休眠5秒
        Thread.sleep(5000);
        //关闭多线程
        sched.shutdown();


    }
}
