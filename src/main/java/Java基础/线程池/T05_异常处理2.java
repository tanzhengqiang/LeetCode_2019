package Java基础.线程池;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 异常处理：重写afterExecute ，然后执行 execute提交

 * ref : https://blog.csdn.net/qq_20009015/article/details/100569976
 */
public class T05_异常处理2 {

    public static void main(String[] args) {

        //1.创建一个自己定义的线程池,重写afterExecute方法
        ExecutorService service = new ThreadPoolExecutor(1, 1,
                0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue(10)) {
            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                super.afterExecute(r, t);
                System.out.println("afterExecute里面获取到异常信息" + t.getMessage());
            }
        };

        //2.提交任务
        service.execute(() -> {
            int i = 1 / 0;
        });


        service.shutdownNow();
    }

}
