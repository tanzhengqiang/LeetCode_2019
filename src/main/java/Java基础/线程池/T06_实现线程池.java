package Java基础.线程池;

/**
 * 实现线程池，需要完成这个核心流程：
 *
 * 线程池中有N个工作线程
 * 把任务提交给线程池运行
 * 如果线程池已满，把任务放入队列
 * 最后当有空闲时，获取队列中任务来执行
 *
 * https://github.com/chenjiabing666/JavaFamily/blob/master/docs/sanfene/javathread.md
 */
public class T06_实现线程池 {
}
