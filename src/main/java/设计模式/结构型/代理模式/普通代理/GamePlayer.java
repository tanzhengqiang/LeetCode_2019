package 设计模式.结构型.代理模式.普通代理;


public class GamePlayer implements IGamePlayer {

    private String name = "";

    // 构造函数 限制谁能创建对象，并同时传递姓名
    public GamePlayer(IGamePlayer gamePlayer, String _name) {
        if (gamePlayer == null) {
            throw new RuntimeException("不能创建真实角色");
        } else {
            this.name = _name;
        }
    }


    @Override
    public void login(String user, String password) {
        System.out.println("登录用户：" + user + "登录成功！");
    }

    @Override
    public void killBoss() {
        System.out.println(this.name + " 在打怪！");
    }

    @Override
    public void upgrade() {
        System.out.println(this.name + ",打怪又升级了！");
    }
}