package 设计模式.结构型.代理模式.普通代理;

public interface IGamePlayer {
    //登录游戏
    void login(String user, String password);

    //杀怪，这是网络游戏的主要特色
    void killBoss();

    //升级
    void upgrade();

}