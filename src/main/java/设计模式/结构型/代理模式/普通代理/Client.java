package 设计模式.结构型.代理模式.普通代理;

/**
 * @Description 普通代理：它的要求就是客户端只能访问代理角色，而不能访问真实角色
 * 调用者只知代理而不用知道真实的角色是谁，
 * 屏蔽了 真实角色的变更对高层模块的影响，
 * 真实的主题角色想怎么修改就怎么修改，对高层次的模块没有任何的影响
 * @Author zhengqiang.tan
 * @Date 2024-10-28
 */
public class Client {
    public static void main(String[] args) {
        IGamePlayer player = new GamePlayerProxy("张三丰");
        player.login("zhangsan", "password");
        player.killBoss();
        player.upgrade();
    }
}