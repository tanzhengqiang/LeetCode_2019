package 设计模式.结构型.代理模式.模板;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @Description
 * @Author zhengqiang.tan
 * @Date 2024-10-28
 */
public class DynamicProxy<T> {
    /**
     * 查找到该类的所有接口，然后实现 接口的所有方法
     * @param classLoader
     * @param interfaces
     * @param handler
     * @return
     * @param <T>
     */
    public static <T> T newProxyInstance(ClassLoader classLoader, Class<?>[] interfaces, InvocationHandler handler) {
        if(true){
            new Client4.MyAdvice().exec();
        }
        return (T) Proxy.newProxyInstance(classLoader, interfaces, handler);
    }
}