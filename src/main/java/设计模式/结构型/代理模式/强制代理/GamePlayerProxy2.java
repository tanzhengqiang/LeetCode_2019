package 设计模式.结构型.代理模式.强制代理;


public class GamePlayerProxy2 implements IGamePlayer, IProxy {

    private IGamePlayer gamePlayer = null;

    public GamePlayerProxy2(IGamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }

    @Override
    public void login(String user, String password) {
        this.gamePlayer.login(user, password);
    }

    @Override
    public void killBoss() {
        this.gamePlayer.killBoss();
    }

    @Override
    public void upgrade() {
        this.gamePlayer.upgrade();
        this.count();
    }


    @Override
    public IGamePlayer getProxy() {
        // 代理的代理暂时没有其他人，那就是自己了，也可以扩展
        return this;
    }

    @Override
    public void count() {
        System.out.println("游戏升级费用：10元");
    }
}