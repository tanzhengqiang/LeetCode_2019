package 设计模式.结构型.代理模式.强制代理;

/**
 * @Description
 * @Author zhengqiang.tan
 * @Date 2024-10-28
 */
public interface IProxy {
    // 计算费用
    void count();
}