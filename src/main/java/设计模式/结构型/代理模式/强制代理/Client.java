package 设计模式.结构型.代理模式.强制代理;

/**
 * @Description 强制代理
 * <p>
 * 强制代理的概念就是要从真实角色查找到代理角色，不允 许直接访问真实角色。
 * 高层模块只要调用getProxy就可以访问真实角色的所有方法，
 * 它根本就不需要产生一个代理出来，代理的管理已经由真实角色自己完成
 * @Version V1.0
 * @Author zhengqiang.tan
 * @Date 2024-10-28
 */
public class Client {
    public static void main(String[] args) {
        // 1. 不使用代理
//        IGamePlayer player = new GamePlayer("张三丰");
//        player.login("zhangsan", "password");
//        player.killBoss();
//        player.upgrade();
//
//        请使用指定的代理访问！
//        请使用指定的代理访问！
//        请使用指定的代理访问！

        // 2. 强制指定代理
        IGamePlayer player = new GamePlayer("张三丰");
        // 肯定也不行，因为是随便 new 出的代理角色
//        IGamePlayer proxy = new GamePlayerProxy(player);
        IGamePlayer proxy = player.getProxy();
        proxy.login("zhangsan", "password");
        proxy.killBoss();
        proxy.upgrade();

//        用户zhangsan登录成功！
//        张三丰 在打怪！
//        张三丰,打怪又升级了！
//        游戏升级费用：10元    --- 做了增强
    }


}