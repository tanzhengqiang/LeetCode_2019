package 设计模式.结构型.代理模式.强制代理;


import 设计模式.结构型.代理模式.强制代理.GamePlayerProxy2;
import 设计模式.结构型.代理模式.强制代理.IGamePlayer;

/**
 * 强制代理
 * 在调用真实角色的时候一定要保证是通过代理类去调用，这样就可以保证强制代理
 */
public class GamePlayer implements IGamePlayer {

    private String name = "";

    private IGamePlayer proxy = null;

    public GamePlayer(String _name) {
        this.name = _name;
    }

    // 找到玩家自己的代理
    @Override
    public IGamePlayer getProxy() {
//        this.proxy = new GamePlayerProxy(this);
        this.proxy = new GamePlayerProxy2(this);
        return this.proxy;
    }


    @Override
    public void login(String user, String password) {
        if (this.isProxy()) {
            System.out.println("用户" + user + "登录成功！");
        } else {
            System.out.println("请使用指定的代理访问！");
        }

    }

    @Override
    public void killBoss() {
        if (this.isProxy()) {
            System.out.println(this.name + " 在打怪！");

        } else {
            System.out.println("请使用指定的代理访问！");
        }
    }


    @Override
    public void upgrade() {
        if (this.isProxy()) {
            System.out.println(this.name + ",打怪又升级了！");
        } else {
            System.out.println("请使用指定的代理访问！");
        }
    }


    // 判断是否是代理访问
    private boolean isProxy() {
        if (this.proxy == null) {
            return false;
        } else {
            return true;
        }
    }
}