package 设计模式.结构型.代理模式.dynamic_type;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Description
 * @Author zhengqiang.tan
 * @Date 2024-10-28
 */
public class GamePlayIH implements InvocationHandler {
    // 被代理者
    Class cls = null;
    // 被代理的实例
    private Object obj;

    // 我要代理谁
    public GamePlayIH(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(this.obj, args);
        // 这就是AOP编程:登录的时候发个通知
        if (method.getName().equalsIgnoreCase("login")) {
            System.out.println("有人在用我的游戏登陆系统了 ");
        }
        return result;
    }
}