package 设计模式.结构型.代理模式.dynamic_type;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @Description
 * 既没有创建代理类，也没有实现IGamePlayer 接口，这就是动态代理
 * @Author zhengqiang.tan
 * @Date 2024-10-28
 */
public class Client3 {
    public static void main(String[] args) {
        GamePlayer player = new GamePlayer("张三");
        InvocationHandler handler = new GamePlayIH(player);

        ClassLoader classLoader = player.getClass().getClassLoader();
        IGamePlayer obj = (IGamePlayer) Proxy.newProxyInstance(classLoader, new Class[]{IGamePlayer.class}, handler);

        obj.login("zhangsan", "1234");
        obj.killBoss();
        obj.upgrade();
    }
}

//object is not an instance of declaring class