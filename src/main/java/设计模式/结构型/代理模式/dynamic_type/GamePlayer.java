package 设计模式.结构型.代理模式.dynamic_type;


public class GamePlayer implements IGamePlayer {

    private String name;

    public GamePlayer(String _name) {
        this.name = _name;
    }

    @Override
    public void login(String user, String password) {
        System.out.println("登录用户：" + user + "登录成功！");
    }

    @Override
    public void killBoss() {
        System.out.println(this.name + " 在打怪！");
    }

    @Override
    public void upgrade() {
        System.out.println(this.name + ",打怪又升级了！");
    }
}