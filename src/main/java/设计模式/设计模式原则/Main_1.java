package 设计模式.设计模式原则;

/**
 * @Title 开闭原则测试1
 * @Author zhengqiang.tan
 * @Date 2021/1/29 8:22 PM
 * 解析：这个方式，则存在则两个缺点。
 * <p>
 * 当我们需要新增新的渠道的时候，需要对main方法中的逻辑进行修改调整。
 * 这违背了设计模式中的开放封闭规则。开放封闭原则的核心的思想是：软件实体是可扩展，而不可修改的。
 * <p>
 * 也就是说，对扩展是开放的，而对修改是封闭的
 * <p>
 * 新增渠道后，修改代码会产生大量的if else，不太优雅。为了解决以上的两个问题，我们可以借助枚举类来巧妙优化。
 */
public class Main_1 {
    public static void main(String[] args) {
        //这里我们模拟接收到的数据，其渠道为为TOUTIAO，来自头条的数据
        String sign = "TOUTIAO";
        GeneralChannelRule rule = null;
        //根据对应渠道获取对应的具体规则实现类
        if (ChannelRuleEnum.TENCENT.channel.equals(sign)) {
            rule = new TencentChannelRule();
        } else if (ChannelRuleEnum.TOUTIAO.channel.equals(sign)) {
            rule = new TouTiaoChannelRule();
        } else {
            //匹配不到
        }
        //执行
        rule.process();
    }
}
