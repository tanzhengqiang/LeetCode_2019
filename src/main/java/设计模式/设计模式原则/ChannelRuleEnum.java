package 设计模式.设计模式原则;

//// 法一
//public enum ChannelRuleEnum {
//    TOUTIAO("TOUTIAO"),
//    TENCENT("Tencent");
//
//    String channel;
//    ChannelRuleEnum(String channel ) {
//        this.channel = channel;
//    }
//
//    public String getChannel() {
//        return channel;
//    }
//}


// 法二
public enum ChannelRuleEnum {
    TOUTIAO("TOUTIAO", new TouTiaoChannelRule()),
    TENCENT("TENCENT", new TencentChannelRule()),
    BAIDU("BAIDU", new BaiduChannelRule()); // NEW ADD

    public String name;
    public GeneralChannelRule channel;

    ChannelRuleEnum(String name, GeneralChannelRule channel) {
        this.name = name;
        this.channel = channel;
    }


    public String getName() {
        return name;
    }

    public GeneralChannelRule getChannel() {
        return channel;
    }

    // 新增匹配方法
    public static ChannelRuleEnum match(String name) {
        ChannelRuleEnum[] values = ChannelRuleEnum.values();
        for (ChannelRuleEnum value : values) {
            if (value.name.equals(name)) {
                return value;
            }
        }
        return null;
    }


}