package 设计模式.设计模式原则;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/1/29 8:00 PM
 */
public class TencentChannelRule extends GeneralChannelRule {
    @Override
    public void process() {
        System.out.println("Deal with Tencent info ...");
    }
}
