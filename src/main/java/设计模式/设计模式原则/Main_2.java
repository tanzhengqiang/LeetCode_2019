package 设计模式.设计模式原则;

/**
 * @Title 开闭原则测试2
 * @Author zhengqiang.tan
 * @Date 2021/1/31 16:32 PM
 * <p>
 * 解析：通过使用枚举类，在枚举中将 key 与 规则具体实现进行绑定。通过改变：
 * <p>
 * 可以减少if -else使得代码更加优雅 如果需要新增渠道，我们只需要在编写具体规则实现类并继承GeneralChannelRule抽象类，
 * 并在枚举类中新增的枚举，而不需要改动到原先的任何代码。这符合了开发封闭原则。
 */
public class Main_2 {
    public static void main(String[] args) {
//        String name = "TOUTIAO";
//        ChannelRuleEnum channelRuleEnum = ChannelRuleEnum.match(name);
//        channelRuleEnum.getChannel().process();

        String name = "BAIDU";
        ChannelRuleEnum.match(name).getChannel().process();
    }
}
