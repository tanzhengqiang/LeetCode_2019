package 设计模式.创建型.单例模式;

/**
 * @Title 懒汉式
 * @Author zhengqiang.tan
 * @Date 2021/3/5 10:48 AM
 */
public class SingleTon_2 {
    private SingleTon_2(){}
    private static  SingleTon_2  singleTon_2 = null;
    public static final SingleTon_2 getInstance()  {
        if (singleTon_2 == null) {
            // Test
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            singleTon_2 = new SingleTon_2();
        }
        return singleTon_2;
    }

    public static void main(String[] args) throws Exception{
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                System.out.println(SingleTon_2.getInstance().hashCode());
            }).start();
        }
    }
}
