package 设计模式.创建型.单例模式;


/**
 * @Title 枚举模式
 * @Author zhengqiang.tan
 * @Date 2021/3/5 10:48 AM
 */
public class  SingleTon_5 {

    private SingleTon_5(){}
    public static SingleTon_5 getInstance() {
        return EnumHolder.INSTANCE.getInstance();
    }

   private enum EnumHolder{
       INSTANCE;
       private  SingleTon_5  instance = null;
       private EnumHolder(){
           instance = new SingleTon_5();
       }
       private SingleTon_5 getInstance(){
           return instance;
       }

   }
    public static void main(String[] args)  {
        for (int i = 0; i < 200; i++) {
            new Thread(() -> {
                System.out.println(SingleTon_5.getInstance().hashCode());
            }).start();
        }
    }
}
