package 设计模式.创建型.单例模式;

/**
 * @Title 懒汉式+双重检查锁
 * @Author zhengqiang.tan
 * @Date 2021/3/5 10:48 AM
 */
public class SingleTon_3 {
    private SingleTon_3() {
    }

    /**
     * 引入volatile关键字修饰实例对象，这是为了避免因JVM指令重排序可能导致的空指针异常。
     * 因为当线程执行到第一个if (null == instance)时，代码可能读取到instance不为null，但此时instance引用的对象可能还没有完成初始化。
     */
    private static volatile SingleTon_3 singleTon_2 = null;

    public static SingleTon_3 getInstance() {
        if (singleTon_2 == null) {
            synchronized (SingleTon_3.class) {
                if (singleTon_2 == null) {
                    singleTon_2 = new SingleTon_3();
                }
            }
        }
        return singleTon_2;
    }

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 200; i++) {
            new Thread(() -> {
                System.out.println(SingleTon_3.getInstance().hashCode());
            }).start();
        }
    }
}
