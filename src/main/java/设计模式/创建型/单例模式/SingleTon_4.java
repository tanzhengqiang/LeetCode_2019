package 设计模式.创建型.单例模式;

/**
 * @Title 静态内部类模式
 * 使用静态内部类实现单例模式是一种比较优雅的方式，它可以保证线程安全，同时也不需要使用同步机制
 * @Author zhengqiang.tan
 * @Date 2021/3/5 10:48 AM
 */
public class SingleTon_4 {
    private SingleTon_4() {
    }
    private static class Holder {
        private static  SingleTon_4 instance = new SingleTon_4();
    }
    public static SingleTon_4 getInstance() {
        return Holder.instance;
    }

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < 200; i++) {
            new Thread(() -> {
                System.out.println(SingleTon_4.getInstance().hashCode());
            }).start();
        }
    }
}
