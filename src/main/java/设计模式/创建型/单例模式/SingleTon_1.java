package 设计模式.创建型.单例模式;

/**
 * @Title 饿汉式 线程安全
 * @Author zhengqiang.tan
 * @Date 2021/3/5 10:42 AM
 */
public class SingleTon_1 {
    private SingleTon_1(){}
    private static SingleTon_1 singleTon = new SingleTon_1();
    public final static SingleTon_1 getInstance(){
        return singleTon;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                System.out.println(SingleTon_1.getInstance().hashCode());
            }).start();
        }
    }

}
