package 设计模式.创建型.抽象工厂;

/**
 * @Title 抽象工厂
 * @Author zhengqiang.tan
 * @Date 2021/2/8 11:23 AM
 */


interface AbstractFactory {
    XiaoMi createXiaoMiPhone();

    HuaWei createHuaWeiPhone();
}


interface XiaoMi {
    String designPhone(String paper);
}

interface HuaWei {
    String designPhone(String paper);

    String dev5G();
}

// ----------富士康 ----------
class FuShiKangProduceXiaoMi implements XiaoMi {

    @Override
    public String designPhone(String paper) {
        return "富士康生产" + paper + "手机";
    }
}

class FuShiKangProduceHuaWei implements HuaWei {

    @Override
    public String designPhone(String paper) {
        return "富士康生产" + paper + "手机";
    }

    @Override
    public String dev5G() {
        return "富士康代工生产华为5G基站";
    }
}

class FuShiKangFactory implements AbstractFactory {

    @Override
    public XiaoMi createXiaoMiPhone() {
        return new FuShiKangProduceXiaoMi();
    }

    @Override
    public HuaWei createHuaWeiPhone() {
        return new FuShiKangProduceHuaWei();
    }
}

// ----------比亚迪 ----------
class BydProduceXiaoMi implements XiaoMi {

    @Override
    public String designPhone(String paper) {
        return "比亚迪生产" + paper + "手机";
    }
}

class BydProduceHuaWei implements HuaWei {

    @Override
    public String designPhone(String paper) {
        return "比亚迪生产" + paper + "手机";
    }

    @Override
    public String dev5G() {
        return "比亚迪代工生产5G产品";
    }
}
class BYDFactory implements AbstractFactory {

    @Override
    public XiaoMi createXiaoMiPhone() {
        return new BydProduceXiaoMi();
    }

    @Override
    public HuaWei createHuaWeiPhone() {
        return new BydProduceHuaWei();
    }
}



public class Main_01 {
    public static void main(String[] args) {
        AbstractFactory factory = new FuShiKangFactory();
        String xiaomi = factory.createXiaoMiPhone().designPhone("xiaomi Note 10");
        System.out.println(xiaomi);
        String huawei = factory.createHuaWeiPhone().designPhone("huawei p30");
        System.out.println(huawei);
        String huaWei5G = factory.createHuaWeiPhone().dev5G();
        System.out.println(huaWei5G);


        AbstractFactory bydFactory = new BYDFactory();
        String xiaomiNote3 = bydFactory.createXiaoMiPhone().designPhone("小米note3");
        System.out.println(xiaomiNote3);
        String p40 = bydFactory.createHuaWeiPhone().designPhone("华为P40");
        System.out.println(p40);
        String dev5G = bydFactory.createHuaWeiPhone().dev5G();
        System.out.println(dev5G);

    }
}