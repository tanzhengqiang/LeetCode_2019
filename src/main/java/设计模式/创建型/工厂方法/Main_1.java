package 设计模式.创建型.工厂方法;

import java.math.BigDecimal;

/**
 * @Title 工厂方法是一种创建型设计模式， 解决了在不指定具体类的情况下创建产品对象的问题。
 *
 *  工厂方法可以隐藏创建产品的细节，且不一定每次都会真正创建产品，完全可以返回缓存的产品，从而提升速度并减少内存消耗。
 * @Author zhengqiang.tan
 * @Date 2021/1/31 9:59 PM
 * <p>
 * https://refactoringguru.cn/design-patterns/factory-method/java/example
 * TODO
 * https://www.liaoxuefeng.com/wiki/1252599548343744/1281319170474017
 * <p>
 * 释义：
 * 工厂方法定义了一个方法， 且必须使用该方法代替通过直接调用构造函数来创建对象 （ new操作符） 的方式。 子类可重写该方法来更改将被创建的对象所属类。
 */
public class Main_1 {
    public static void main(String[] args) {
        // 调用方可以完全忽略真正的工厂NumberFactoryImpl和实际的产品BigDecimal，
        // 这样做的好处是允许创建产品的代码独立地变换，而不会影响到调用方
        NumberFactory factory = NumberFactory.getFactory();
        Number result = factory.parse("123.456");

    }
}

/**
 * 抽象工厂
 */
interface NumberFactory {
    // 创建方法:
    Number parse(String s);

    // 获取工厂实例:
    static NumberFactory getFactory() {
        return impl;
    }

    NumberFactory impl = new NumberFactoryImpl();
}


class NumberFactoryImpl implements NumberFactory {
    public Number parse(String s) {
        return new BigDecimal(s);
    }
}


/**
 * 静态工厂
 * 应用广泛：Integer n = Integer.valueOf(100);
 */
class NumberFactory_01 {
    public static Number parse(String s) {
        return new BigDecimal(s);
    }
}
