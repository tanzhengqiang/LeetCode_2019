package 设计模式.行为型.策略模式.v1;

/**
 * @Description 策略模式扩展实现（浓缩策略模式的 枚举）
 * @Author zhengqiang.tan
 * @Date 2024-11-01
 */
public class StrategyExtMain {

    /**
     * 枚举实现(策略模式+枚举)
     */
    enum Calculate {
        ADD("+") {
            public int exec(int num1, int num2) {
                return num1 + num2;
            }
        },
        SUB("-") {
            public int exec(int num1, int num2) {
                return num1 - num2;
            }

        };
        String value = "";

        private Calculate(String value) {
            this.value = value;
        }

        abstract int exec(int num1, int num2);

    }


    /**
     * 这个策略模式用的真爽啊～
     *
     * @param args
     */
    public static void main(String[] args) {
        int num1 = 1;
        int num2 = 2;
        System.out.println(Calculate.ADD.exec(num1, num2));
        System.out.println(Calculate.SUB.exec(num1, num2));

    }


}