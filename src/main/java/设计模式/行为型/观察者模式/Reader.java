package 设计模式.行为型.观察者模式;

import javax.validation.constraints.NotNull;
import java.util.Observable;
import java.util.Observer;

/**
 * @Title 定义观察者（订阅者）
 * @Author zhengqiang.tan
 * @Date 2021/3/31 3:00 PM
 */
public class Reader implements Observer {
    @NotNull
    private String name;

    public Reader(String name) {
        this.name = name;
    }
    @Override
    public void update(Observable o, Object arg) {
        ArticlePublish articlePublish = (ArticlePublish) o;
        String msg = articlePublish.getAticle();
        System.out.printf("读者：%s 收到文章：%s \n", name, msg);

    }
}
