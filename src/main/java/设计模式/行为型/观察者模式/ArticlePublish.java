package 设计模式.行为型.观察者模式;

import java.util.Observable;

/**
 * @Title 定义被观察对象
 * @Author zhengqiang.tan
 * @Date 2021/3/31 2:57 PM
 * https://mp.weixin.qq.com/s/nEYhChMpLQhdZmTuo8jW6A
 */
public class ArticlePublish extends Observable {

    private String article;

    public void publish(String article) {
        // 1. 发文章
        this.article = article;
        // 2. 改状态
        this.setChanged();
        // 3. 通知订阅者（观察者）
        this.notifyObservers();

    }

    public String getAticle() {
        return article;
    }

    public void setAticle(String article) {
        this.article = article;
    }
}
