package 设计模式.行为型.观察者模式;

/**
 * @Title 测试用例
 * https://github.com/javastacks/javastack
 * @Author zhengqiang.tan
 * @Date 2021/3/31 3:03 PM
 */
public class ObserverTest {
    public static void main(String[] args) {
        ArticlePublish articlePublish = new ArticlePublish();

        articlePublish.addObserver(new Reader("xiaoming"));
        articlePublish.addObserver(new Reader("xiaoliang"));
        articlePublish.addObserver(new Reader("xiaoqiang"));

        articlePublish.publish("文章：观察者模式介绍");


    }
}

//观察者模式的优点是为了给观察目标和观察者解耦，而缺点也很明显，从上面的例子也可以看出，
// 如果观察者对象太多的话，有可能会造成内存泄露。
//
//另外，从性能上面考虑，所有观察者的更新都是在一个循环中排队进行的，
// 所以观察者的更新操作可以考虑做成线程异步（或者可以使用线程池）的方式，以提升整体效率
