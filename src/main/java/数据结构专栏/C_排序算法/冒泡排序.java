package 数据结构专栏.C_排序算法;

import java.util.Arrays;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/5/13 11:00 AM
 */
public class 冒泡排序 {

    public static void main(String[] args) {
        int arr[] = {1, 3, 5, 9, 4, 2};
        System.out.println(Arrays.toString(arr));

        bubbleSort(arr);

        String s = Arrays.toString(arr);
        System.out.println(s);


    }

    // 相邻位置两两比较并交换
    public static void bubbleSort(int arr[]) {
        int N = arr.length;

        for (int i = 0; i < N - 1; i++) {
            for (int j = 0; j < N - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }


}
