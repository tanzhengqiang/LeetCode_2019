package 数据结构专栏.D_数组;

/**
 * 输入：nums = [1,1,1,2,2,3]
 * 输出：5, nums = [1,1,2,2,3]
 * 解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3 。 不需要考虑数组中超出新长度后面的元素。
 */
public class 删除数组中的重复项II {
    public static void main(String[] args) {


    }


    /**
     * https://zhuanlan.zhihu.com/p/366701713
     * 时间复杂度：O(n)，其中 n 是数组的长度。我们最多遍历该数组一次。
     * 空间复杂度：O(1)。我们只需要常数的空间存储若干变量。
     *
     * @param nums
     * @return
     */
    public static int removeDuplicates(int[] nums) {
        int n = nums.length;
        if (n <= 2) {
            return n;
        }
        int slow = 2, fast = 2;
        while (fast < n) {
            if (nums[slow - 2] != nums[fast]) {
                nums[slow] = nums[fast];
                ++slow;
            }
            ++fast;
        }
        return slow;
    }
}
