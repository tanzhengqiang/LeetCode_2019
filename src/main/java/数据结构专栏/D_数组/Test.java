package 数据结构专栏.D_数组;

import java.util.Stack;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/5/20 9:27 PM
 * <p>
 * 面试： -120 -> -21
 * 1234-> 4321
 */
public class Test {
    public static void main(String[] args) {
        int t1 = -120;
//        int t1 = 1234;
        System.out.println(getReverseInt2(t1));

    }


    // 我是用栈实现的
    public static int getReverseIn1(int num) {
        boolean flag = false;
        if (num < 0) {
            flag = true;
        }
        int n = Math.abs(num);
        String str = String.valueOf(n);
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) != '0') {
                stack.push(Integer.parseInt(String.valueOf(str.charAt(i))));
            }
        }
        StringBuilder sb = new StringBuilder();
        int size = stack.size();

        for (int i = 0; i < size; i++) {
            sb.append(stack.pop());
        }

        if (flag) {
            return Integer.parseInt("-" + sb.toString());
        } else {
            return Integer.parseInt(sb.toString());
        }

    }

    public static int getReverseInt2(int num) {
        if (num < 0) {
            int t = Math.abs(num);
            return Integer.parseInt("-" + new StringBuilder(t + "").reverse().toString());
        } else {
            return Integer.parseInt(new StringBuilder(num + "").reverse().toString());
        }
    }


}
