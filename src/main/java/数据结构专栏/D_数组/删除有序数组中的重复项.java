package 数据结构专栏.D_数组;

import java.util.Arrays;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/4/22 11:27 AM
 */
public class 删除有序数组中的重复项 {
    public static void main(String[] args) {
        int[] nums = {1,2,2,3,3,5};
        delRepeatArray(nums);
        System.out.println(Arrays.toString(nums));

    }


    // ??
    public static void delRepeatArray(int[] nums) {
        int tmp = nums[0];
        int len = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != tmp) {
                nums[len] = nums[i];
                tmp = nums[i];
                len++;
            }
        }
    }

}
