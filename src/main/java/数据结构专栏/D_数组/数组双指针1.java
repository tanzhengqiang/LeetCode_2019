package 数据结构专栏.D_数组;

import java.util.Arrays;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/6/24 10:54 PM
 * <p>
 * 输入：nums = [1,2,3,4]
 * 输出：[1,3,2,4]
 * 注：[3,1,2,4] 也是正确的答案之一。
 * <p>
 * <p>
 * 1 <= nums.length <= 50000
 * 1 <= nums[i] <= 10000
 * <p>
 * 文章参考：
 * https://mp.weixin.qq.com/s?__biz=MzI2NjA3NTc4Ng==&mid=2652088296&idx=2&sn=076e503becd621f65010169bf1374756&chksm=f174ac0dc603251b2a56bfd56a5a8a075a41881b7f999778cbc80cb37492fec11937490c0605
 */
public class 数组双指针1 {
    public static void main(String[] args) {
        System.out.println(5 & 1);//1
        System.out.println(6 & 1);//0

        int[] nums = new int[]{1,2, 7,8};

        System.out.println(Arrays.toString(exchange(nums)));

    }

    public static  int[] exchange(int[] nums) {
        // 定义头指针 left ，尾指针 right ,临时变量 tmp
        int left = 0, right = nums.length - 1, tmp;

        //重复操作，直到 left == right 为止
        while (left < right) {
            // nums[left] & 1 与 nums[right] & 1 都是用来判断数字奇偶性。
            // left 一直往右移，直到它指向的值为偶数
            while (left < right && (nums[left] & 1) == 1) left++;
            // right 一直往左移， 直到它指向的值为奇数
            while (left < right && (nums[right] & 1) == 0) right--;

            // 交换 nums[left] 和 nums[right]*
            tmp = nums[left];
            nums[left] = nums[right];
            nums[right] = tmp;
        }
        return nums;
    }

}
