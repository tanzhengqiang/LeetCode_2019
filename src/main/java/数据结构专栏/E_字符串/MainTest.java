package 数据结构专栏.E_字符串;

import java.util.LinkedList;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/5/18 8:19 AM
 */
public class MainTest {
    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
        String s1 = "abcdef", s2 = "cd";
        System.out.println(strContains(s1, s2));
    }


    // 1. 求最长子串 输入: s = "abcabcbb" 返回 3
    // 使用LinkedList来装的，便于插入和删除，效率高，效率提高，运行速度提升，
    public static int lengthOfLongestSubstring(String s) {
        int N = s.length();
        if (N == 0) {
            return 0;
        }
        if (s.trim().length() == 0) {
            return 1;
        }
        LinkedList<Character> list = new LinkedList();
        int result = 0;
        for (int i = 0; i < N; i++) {
            char str = s.charAt(i);
            if (list.contains(str)) {
                if (result < list.size()) {
                    result = list.size();
                }
                while (str != list.removeFirst()) {
                }
            }
            list.addLast(str);
        }
        if (result < list.size()) {
            result = list.size();
        }
        return result;

    }


    // 考试 判断字符串是否包含
    public static boolean strContains(String s1, String s2) {
        int s1Len = s1.length();
        int s2Len = s2.length();
        for (int i = 0; i < s1Len - s2Len + 1; i++) {
            return s1.substring(i, i+s2Len).equals(s2);
        }
        return false;
    }


    public static boolean isContains(String t, String s) {
        //判null判空
        if (s == null || t == null) return false;
        if (s == "") return true;
        if (t == "" && s == "") return true;
        if (t == "") return false;

        int countT = 0;//包含字符串匹配次数
        int countS = 0;//被包含字符串匹配次数
        char ta, sa;//字符串中的每一个字母

        for (int i = 0; i < t.length(); i++) {
            ta = t.charAt(i);
            sa = s.charAt(countS);
            if (ta == sa && countS < s.length())
                countS++;
            if (countS != 0)
                countT++;
            if (countS == s.length())//被包含字符串匹配完毕
                break;
        }

        if (countS == s.length() && countS == countT) {
            return true;
        }
        else{
            return false;
        }
    }



}
