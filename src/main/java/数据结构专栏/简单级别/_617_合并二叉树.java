package 数据结构专栏.简单级别;

/**
 * @DESC 给定两个二叉树，想象当你将它们中的一个覆盖到另一个上时，两个二叉树的一些节点便会重叠。
 * https://leetcode-cn.com/problems/merge-two-binary-trees/
 * @Author tzq
 * @Date 2020-04-14 09:43
 **/
public class _617_合并二叉树 {


    /**
     * 官方解答：
     * 终止条件：树1的节点为null，或者树2的节点为null
     * 递归函数内：将两个树的节点相加后，再赋给树1的节点。再递归的执行两个树的左节点，递归执行两个树的右节点
     * <p>
     * 相当于将左树定位初始树依据条件是否为空进行值相加
     *
     * @param t1
     * @param t2
     * @return
     */
    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null) return t2;
        if (t2 == null) return t1;
        t1.val += t2.val;
        t1.left = mergeTrees(t1.left, t2.left);
        t1.right = mergeTrees(t1.right, t2.right);
        return t1;
    }



    /**
     * 我的解答
     *
     * @param t1
     * @param t2
     * @return
     */
    public TreeNode mergeTrees2(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) return null;
        if (t1 == null || t2 == null) return t1 == null ? t2 : t1;
        if (t1 != null && t2 != null) {
            t1.val = t1.val + t2.val;
        }
        t1.left = mergeTrees2(t1.left, t2.left);
        t1.right = mergeTrees2(t1.right, t2.right);
        return t1;

    }


    // 如果使用迭代算法如何来写呢？
}
