package 数据结构专栏.简单级别;

/**
 * @DESC TODO
 * @Author tzq
 * @Date 2020-03-27 16:22
 **/
public class _520_检测大写字母 {
    public static void main(String[] args) {

    }

    public static boolean detectCapitalUse(String word) {
        int len = word.length();
        int cnt = 0;
        for (int i = 0; i < len; i++) {
            if (isUpperCase(word.charAt(i))) {
                cnt++;
            }
        }
        // 全是大写或小写 返回true
        if (cnt == 0 || cnt == len) {
            return true;
            //首字母大写且count大写字母个数等于1 返回true
        } else if (isUpperCase(word.charAt(0)) && cnt == 1) {
            return true;
        }
        // 其他情况返回 false
        return false;
    }


    private static boolean isUpperCase(char c) {
        if (c >= 'A' && c <= 'Z') {
            return true;
        }
        return false;
    }
}
