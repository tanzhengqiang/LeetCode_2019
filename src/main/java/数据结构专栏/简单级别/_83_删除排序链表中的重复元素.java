package 数据结构专栏.简单级别;

import 数据结构专栏.B_链表专栏.ListNode;

/**
 * @DESC https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/
 * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
 * @Author tzq
 * @Date 2020-03-31 21:22
 **/
public class _83_删除排序链表中的重复元素 {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(1);
        ListNode l3 = new ListNode(2);
        l1.next = l2;
        l2.next = l3;


    }


    /**
     * 方法：直接法
     * 复杂度分析
     * 时间复杂度：O(n)，因为列表中的每个结点都检查一次以确定它是否重复，所以总运行时间为 O(n)O(n)，其中 nn 是列表中的结点数。
     * 空间复杂度：O(1)，没有使用额外的空间
     * https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/solution/hua-jie-suan-fa-83-shan-chu-pai-xu-lian-biao-zhong/
     *
     * @param head
     * @return
     */
    public ListNode deleteDuplicates(ListNode head) {
        ListNode curr = head;
        while (curr.next != null && curr != null) {
            if (curr.next.val == curr.val) {
                curr.next = curr.next.next;
            } else {
                curr = curr.next;
            }
        }
        return head;
    }


    public ListNode deleteDuplicates2(ListNode head) {
        if (head == null || head.next == null) return head;

        head.next = deleteDuplicates(head.next);
        if (head.val == head.next.val) {
            head = head.next;
        }
        // head是头指针, 是不会发生变动的，current是临时指针，对链表的操作就是操作current
        return head;

    }
}
