package 数据结构专栏.简单级别;

/**
 * @DESC https://leetcode-cn.com/problems/roman-to-integer/
 * @Author tzq
 * @Date 2020-03-24 17:55
 * <p>
 * 字符          数值
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * <p>
 * IV=4
 * IX=9
 * LVIII=58 50+5+3
 * 输入: "MCMXCIV" 输出: 1994  M = 1000, CM = 900, XC = 90, IV = 4.
 **/
public class _13_罗马数字转整数 {
    public static void main(String[] args) {

        String s = "LVIII";
        System.out.println(romanToInt(s));
    }


    public static int romanToInt(String s) {

        int sum = 0;
        int p1 = getValue(s.charAt(0));
        for (int i = 1; i < s.length(); i++) {
            int value = getValue(s.charAt(i));

            if(p1 < value) {
                sum -= p1;
            } else {
                sum += p1;
            }

            p1 = value;

        }
        sum += p1;
        return sum;

    }


    private static int getValue(char c) {
        switch (c) {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }
}
