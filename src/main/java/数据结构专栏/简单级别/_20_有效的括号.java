package 数据结构专栏.简单级别;

import java.util.Stack;

/**
 * @DESC https://leetcode-cn.com/problems/valid-parentheses/
 * @Author tzq
 * @Date 2020-03-26 13:13
 * 面过
 **/
public class _20_有效的括号 {

    public static void main(String[] args) {
        System.out.println(isValid("()[]{}")); //true
        System.out.println(isValid("([)]")); //false
        System.out.println(isValid("{[]}")); // true
    }


    /**
     * 联想JAVA编译器的处理模式：栈结构
     *
     * @param s
     * @return
     */
    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        if (s == null || s.length() == 0) {
            return true;
        }

        for (int i = 0; i < s.length(); i++) {
            if (stack.isEmpty()) {
                stack.push(s.charAt(i));
            } else {
                if (stack.peek() == getValue(s.charAt(i))) {
                    stack.pop();
                } else {
                    stack.push(s.charAt(i));
                }
            }
        }
        return stack.isEmpty() ? true : false;
    }

    private static char getValue(char c) {
        switch (c) {
            case '{':
                return '}';
            case '}':
                return '{';
            case '(':
                return ')';
            case ')':
                return '(';
            case '[':
                return ']';
            case ']':
                return '[';
            default:
                return '1';
        }
    }

}
