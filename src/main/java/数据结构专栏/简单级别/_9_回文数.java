package 数据结构专栏.简单级别;

/**
 * @DESC https://leetcode-cn.com/problems/palindrome-number/
 * 判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
 * @Author tzq
 * @Date 2020-03-23 10:05
 **/
public class _9_回文数 {
    public static void main(String[] args) {
//        System.out.println(isPalindrome(9));
//        System.out.println(isPalindrome(121));
//        System.out.println(isPalindrome(-121));
//        System.out.println(isPalindrome(111));

        System.out.println(isPalindrome3(11211));

    }


    /**
     * 我的解法
     *
     * @param x
     * @return
     */
    public static boolean isPalindrome(int x) {
        String o1 = String.valueOf(x);
        String o2 = new StringBuilder(o1).reverse().toString();
        if (o1.equals(o2)) {
            return true;
        }

        return false;

    }

    /**
     * https://leetcode-cn.com/problems/palindrome-number/solution/dong-hua-hui-wen-shu-de-san-chong-jie-fa-fa-jie-ch/
     *
     * @param x
     * @return
     */
    public boolean isPalindrome2(int x) {
        String reversedStr = (new StringBuilder(x + "")).reverse().toString();
        return (x + "").equals(reversedStr);
    }


    /**
     * 2.通过取整和取余操作获取整数中对应的数字进行比较。
     */
    public static boolean isPalindrome3(int x) {
        if (x < 0) return false;
        int div = 1;
        while (x / div >= 10) div *= 10;

        while (x > 0) {
            int left = x / div;
            int right = x % 10;
            if (left != right) return false;
            x = (x % div) / 10;
            div /= 100;
        }
        return true;
    }


}
