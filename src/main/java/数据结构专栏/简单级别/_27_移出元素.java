package 数据结构专栏.简单级别;

/**
 * @DESC 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 * <p>
 * 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
 * <p>
 * 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 * <p>
 * https://leetcode-cn.com/problems/remove-element/
 * <p>
 * 给定 nums = [3,2,2,3], val = 3,
 * 函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。
 * @Author tzq
 * @Date 2020-03-27 11:11
 **/
public class _27_移出元素 {
    public static void main(String[] args) {
        int[] arr = new int[]{3, 2, 2, 3};
        int[] arr2 = new int[]{0, 1, 2, 2, 3, 0, 4, 2};
        int[] arr3 = new int[]{2, 2, 2};
        System.out.println(removeElement2(arr, 2));
        System.out.println(removeElement2(arr2, 2));
        System.out.println(removeElement2(arr3, 2));


    }

    /**
     * 正确解法 3 2 2 3示例
     *
     * n(j=0)=3  != val, 则n(i=0)=n(j=0)=3，i++, n(i=1)直到 n(j)!= val ,n(i=1)=n(j=x),i++
     *
     * @param nums
     * @param val
     * @return
     */
    public static int removeElement2(int[] nums, int val) {
        if (nums.length == 0) return 0;
        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[i] = nums[j];
                i++;
            }
        }
        return i;

    }


    public static int removeElement(int[] nums, int val) {
        if (nums.length == 0) return 0;
        int cnt = 0;
        for (int i = 0; i < nums.length; i++) {
            if (val == nums[i]) {
                cnt += 1;
            }
        }
        return nums.length - cnt;
    }
}
