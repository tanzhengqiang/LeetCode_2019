package 数据结构专栏.简单级别;

/**
 * @Title 求一个字符串中最长的连续出现的字符，输出该字符及其出现次数，字符串中无空白字符（空格、回车和tab），如果这样的字符不止一个，则输出第一个。
 * @Author zhengqiang.tan
 * @Date 2021/4/12 7:15 PM
 */
public class 字符串中最长的连续出现的字符 {
    public static void main(String[] args) {
        String str = "aabbbcccc";// 定义的字符串

        char[] c = str.toCharArray();// 分割成数组
        int max = 0;// 定义一个记住最大次数的变量
        char cc = 0;// 定义一个保存出现最多次数的字符

        for (int i = 0; i < c.length; i++) {// 循环比较
            int is = 1;// 定义一个中间值
            for (int j = i; j < c.length - 1; j++) {
                if (c[j] == c[j + 1]) {
                    is++;
                } else {
                    break;
                }
                if (is > max) {
                    max = is;
                    cc = c[i];
                }
            }
        }

        System.out.println("出现次数最多的是" + cc + ",最多连续出现" + max + "次");

    }
}
