package 数据结构专栏.简单级别;

import 数据结构专栏.B_链表专栏.ListNode;

/**
 * @DESC
 * 给定一个链表，两两交换其中相邻的节点，并返回交换后的链表。
 * https://leetcode-cn.com/problems/swap-nodes-in-pairs/
 * @Author tzq
 * @Date 2020-03-31 21:56
 **/
public class _24_两两交换链表中的节点 {
    public static void main(String[] args) {

    }

    public ListNode swapPairs(ListNode head) {

        if (head.next ==null|| head == null) return head;

        // head next swapPairs(next.next)
        //下面的任务便是交换这3个节点中的前两个节点
        ListNode next = head.next;
        head.next = swapPairs(next.next);
        next.next = head;
        //根据第二步：返回给上一级的是当前已经完成交换后，即处理好了的链表部分
        return next;
    }
}
