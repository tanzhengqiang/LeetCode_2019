package 数据结构专栏.简单级别;

/**
 * @DESC TODO
 * @Author tzq
 * @Date 2020-04-01 10:25
 **/
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int x) { val = x; }
}