package 数据结构专栏.简单级别;

/**
 * @DESC https://leetcode-cn.com/problems/same-tree/
 * 给定两个二叉树，编写一个函数来检验它们是否相同。
 * @Author tzq
 * @Date 2020-04-14 20:16
 **/
public class _100_相同的树 {
    public static void main(String[] args) {
        TreeNode p0 = new TreeNode(1);
        TreeNode p1 = new TreeNode(2);
        TreeNode p2 = new TreeNode(3);
        p0.left = p1;
        p0.right = p2;

        TreeNode q0 = new TreeNode(1);
        TreeNode q1 = new TreeNode(2);
        TreeNode q2 = new TreeNode(3);

        q0.left = q1;
        q0.right = q2;

        System.out.println(new _100_相同的树().isSameTree(p0, q0));

    }

    /**
     * 我的解法
     *
     * @param p
     * @param q
     * @return
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) return true;
        if (p == null || q == null) return false;
        if (p.val != q.val) return false;
        boolean b1 = isSameTree(p.left, q.left);
        boolean b2 = isSameTree(p.right, q.right);
        if (b1 && b2) {
            return true;
        }
        return false;

    }
}
