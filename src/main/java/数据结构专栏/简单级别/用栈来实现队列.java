package 数据结构专栏.简单级别;

import java.util.Stack;

/**
 * @Title 栈实现队列
 * 队列：先进先出
 * 栈： 先进后出
 * @Author zhengqiang.tan
 * @Date 2021/5/13 2:28 PM
 */
public class 用栈来实现队列 {

    /**
     * 定义两个栈来实现队列
     * 栈A 负责插入新元素
     * 栈B 负责移除老元素
     */
    private Stack<Integer> stackA = new Stack<>();
    private Stack<Integer> stackB = new Stack<>();

    /**
     * 入队操作
     *
     * @Param element
     */
    public void enQueue(int element) {
        stackA.push(element);
    }


    /**
     * 出队操作
     */
    public Integer deQueue() {
        if (stackB.isEmpty()) {
            if (stackA.isEmpty()) {
                return null;
            }
            // 从stackA栈中拿到出栈元素压入栈B
            while (!stackA.isEmpty()) {
                stackB.push(stackA.pop());
            }
        }
        return stackB.pop();
    }


    public static void main(String[] args) {
        用栈来实现队列 stackQueue = new 用栈来实现队列();
        stackQueue.enQueue(1);
        stackQueue.enQueue(2);
        stackQueue.enQueue(3);

        System.out.println(stackQueue.deQueue());
        System.out.println(stackQueue.deQueue());
        System.out.println(stackQueue.deQueue());

        stackQueue.enQueue(4);
        System.out.println(stackQueue.deQueue());


    }

}
