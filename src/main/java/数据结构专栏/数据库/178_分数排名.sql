Q:编写一个 SQL 查询来实现分数排名。如果两个分数相同，则两个分数排名（Rank）相同。请注意，平分后的下一个名次应该是下一个连续的整数值。换句话说，名次之间不应该有“间隔”。

+----+-------+
| Id | Score |
+----+-------+
| 1  | 3.50  |
| 2  | 3.65  |
| 3  | 4.00  |
| 4  | 3.85  |
| 5  | 4.00  |
| 6  | 3.65  |
+----+-------+
例如，根据上述给定的 Scores 表，你的查询应该返回（按分数从高到低排列）：

+-------+------+
| Score | Rank |
+-------+------+
| 4.00  | 1    |
| 4.00  | 1    |
| 3.85  | 2    |
| 3.65  | 3    |
| 3.65  | 3    |
| 3.50  | 4    |
+-------+------+



#我的解法
select a.score as Score,t.rk as Rank from scores a,
(select score,@count:= @count +1 as rk
    from (
        select @count:=0,score from scores group by score order by score desc
    ) tmp order by rk
)t
where a.score = t.score
order by t.rk asc;



-- select a.score as Score, count(distinct b.score) as Rank
-- from scores a, scores b
-- where b.score >= a.score
-- group by a.id
-- order by a.score desc;

--mysql貌似没有这个函数，提示报错
--select score, dense_rank() over (order by score desc) as Rank from scores;