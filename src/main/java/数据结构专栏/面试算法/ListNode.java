package 数据结构专栏.面试算法;

/**
 * @DESC TODO
 * @Author tzq
 * @Date 2020-03-30 19:14
 **/
public class ListNode {
    public int data;
    public ListNode next;

    public ListNode(int data) {
        this.data = data;
    }

    public ListNode(int data, ListNode next) {
        this.data = data;
        this.next = next;
    }
}
