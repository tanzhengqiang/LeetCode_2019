package 数据结构专栏.面试算法;

/**
 * @DESC https://www.cnblogs.com/gl-developer/p/6438311.html
 * @Author tzq
 * @Date 2020-03-29 21:28
 **/
public class _面试题5_从头到尾打印链表 {

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(3);
        ListNode l3 = new ListNode(4);
        l1.next = l2;
        l2.next = l3;
        printList(l1);

    }

    /**
     * 正序打印
     *
     * @param root
     */
    public static void printList(ListNode root) {
        if (root == null) return;
        if (root.next != null) {
            System.out.print(root.data + "->");
        } else {
            System.out.println(root.data);
        }
        printList(root.next);


    }
}
