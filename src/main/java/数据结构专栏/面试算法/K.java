package 数据结构专栏.面试算法;

import java.util.PriorityQueue;

/**
 * @Title 返回倒数弟K大的元素
 * @Author zhengqiang.tan
 * @Date 2021/4/21 3:34 PM
 * https://baijiahao.baidu.com/s?id=1665383380422326763&wfr=spider&for=pc
 *
 * 优先级队列使用二叉堆的特点，可以使得插入的数据自动排序（升序或者是降序）。
 *
 */
public class K {
    public static void main(String[] args) {
        int [] nums = {1,8,4,6,5,3,2};
        System.out.println(finkK(nums,3));
    }

    public static int finkK(int[] nums, int k) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for (int num : nums) {
            queue.add(num);
            if (queue.size() > k) {
                Integer v = queue.poll();
                System.out.println("取出并移出队首元素：" +v);
            }
        }
        return queue.peek();
    }
}
