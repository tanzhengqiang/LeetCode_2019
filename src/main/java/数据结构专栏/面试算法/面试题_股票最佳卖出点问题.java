package 数据结构专栏.面试算法;

/**
 * 假设给定一个数组，其中第i个元素表示第i天的股票价格，请设计一个算法计算出最佳的卖出点，即卖出股票后能获得的最大利润。
 * 输入：[7,1,5,3,6,4]
 * 输出：5
 */
public class 面试题_股票最佳卖出点问题 {
    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length < 2) {
            return 0; // 如果数组为空或长度小于2，无法进行买卖，返回0
        }

        int minPrice = prices[0]; // 初始化最小价格为数组的第一个元素
        int maxProfit = 0; // 初始化最大利润为0

        for (int i = 1; i < prices.length; i++) {
            // 更新最小价格
            if (prices[i] < minPrice) {
                minPrice = prices[i];
            } else {
                // 计算当前价格与最小价格的差值，更新最大利润
                int profit = prices[i] - minPrice;
                maxProfit = Math.max(maxProfit, profit);
            }
        }
        return maxProfit; // 返回最大利润
    }

}
