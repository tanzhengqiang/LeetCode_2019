package 数据结构专栏.A_二叉树专栏.E_前缀树;

/**
 * @Description TODO
 * @Created by zhengqiang
 * @Date 2023/1/11 19:46
 */
public class Test {
    public static void main(String[] args) {
        // char 在Java中是2个字节。Java采用unicode，2个字节(16位)来表示一个字符
        // char + char，char + int——类型均提升为int，附值char变量后，输出字符编码表中对应的字符。
        char a = 'a';
        System.out.println('c' - 'a'); // 2
        System.out.println('b' + 'a'); // 195
    }
}
