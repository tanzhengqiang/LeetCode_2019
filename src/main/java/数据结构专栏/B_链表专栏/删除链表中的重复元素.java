package 数据结构专栏.B_链表专栏;

/**
 * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
 * <p>
 * 示例 1:
 * <p>
 * 输入: 1->1->2
 * 输出: 1->2
 * 示例 2:
 * <p>
 * 输入: 1->1->2->3->3
 * 输出: 1->2->3
 * 考过
 */
public class 删除链表中的重复元素 {

    public static void main(String[] args) {
        ListNode list = ListUtil.getList();
        ListUtil.printList(list);
        ListUtil.printList(deleteDuplicates(list));
//        1->2->2->4->5
//        1->2->4->5

    }


    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        head.next = deleteDuplicates(head.next);
        if (head.val == head.next.val) {
            head = head.next;
        }
        return head;
    }


    public ListNode deleteDuplication(ListNode pHead) {
        if (pHead == null) return null;
        if (pHead != null && pHead.next == null) return pHead;

        ListNode temp = new ListNode(-1);
        temp.next = pHead;
        ListNode tail = temp;
        ListNode p = pHead;
        while (p != null && p.next != null) {
            if (p.val == p.next.val) {
                int value = p.val;
                // 把重复值拿出来，方便比较，这样p指针就是当前指针,如果不用这个 需要比较P 和 p.next处理较麻烦
                while (p != null && value == p.val) {
                    p = p.next;
                }
                tail.next = p;
            } else {
                tail = p;
                p = p.next;
            }
        }
        return temp.next;
    }

}
