package 数据结构专栏.B_链表专栏;

/**
 * @DESC ListNode 我是一个孤独的单链表
 * @Author tzq
 * @Date 2020-03-31 21:23
 **/
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }
}
