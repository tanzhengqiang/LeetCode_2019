package 数据结构专栏.B_链表专栏;

/**
 * @Title 判断单链表是否有环
 * @Author zhengqiang.tan
 * @Date 2021/4/17 6:14 PM
 */
public class 判断单链表是否有环 {

    public static boolean hasLoop(ListNode n) {
        //定义两个指针p1,p2
        ListNode p1 = n;
        ListNode p2 = n.next;
        while (p2 != null) {
            p1 = p1.next;  //每次迭代时，指针1走一步，指针2走两步
            p2 = p2.next.next;
            if (p2 == null) return false;//不存在环时，退出
            if (p1.val == p2.val) return true;//当两个指针重逢时，说明存在环，否则不存在。
        }
        return true; //如果p2为null，说明元素只有一个，也可以说明是存在环
    }


}
