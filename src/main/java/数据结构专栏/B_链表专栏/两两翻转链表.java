package 数据结构专栏.B_链表专栏;

/**
 * @Title 如题翻转链表，1，2，3，4，5 -> 2，1，4，3，5
 * @Author zhengqiang.tan
 * @Date 2021/4/21 4:04 PM
 */
public class 两两翻转链表 {

   /**注意判断需要保存下 下一个节点，以及pre，用来保存两两翻转后1指向4*/
    public ListNode reverseDoubleList(ListNode head) {
        if (head == null || head.next == null) return head;

        ListNode p = head;
        ListNode res = p.next; // 保存返回的链表头

        ListNode pre = null;
        ListNode pnextnext = null;

        while (p != null && p.next != null) {
            pnextnext = p.next.next;
            p.next.next = p;

            // 注意判断是否是第一次，第一次的pre不会自动向前走
            if (pre == null) {
                pre = p;
            } else {
                pre.next = p.next;
            }
            p.next = pnextnext;

            pre = p;
            p = p.next;
        }
        return res;
    }

}
