package 数据结构专栏.B_链表专栏;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/4/17 9:48 PM
 */
public class ListUtil {


    /**
     * 打印单链表
     *
     * @param node
     */
    public static void printList(ListNode node) {
        while (node != null) {
            if (node.next != null) {
                System.out.print(node.val + "->");
            } else {
                System.out.println(node.val);
            }
            node = node.next;
        }
    }

    public static void printList2(ListNode node) {
        if (node == null) return;
        if (node.next != null) {
            System.out.print(node.val + "->");
        } else {
            System.out.println(node.val);
        }
        printList2(node.next);
    }

    /**
     * 含有重复元素的单链表
     *
     * @return
     */
    public static ListNode getList() {
        ListNode head = new ListNode(0);
        ListNode curr = null;
        ListNode last = head;
        for (int i = 1; i < 10; i++) {
            curr = new ListNode(i);
            last.next = curr;
            last = curr;
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode list = getList();
        printList(list);
    }
}
