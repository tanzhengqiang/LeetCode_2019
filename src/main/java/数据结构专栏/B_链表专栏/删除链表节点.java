package 数据结构专栏.B_链表专栏;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/4/17 5:37 PM
 *
 *
 * LCR 136. 删除链表的节点  简单
 * 注意：
 * 1、输入的链表至少包括两个节点
 * 2、链表中每个结点的值都是唯一的
 * 3、输入的待删除节点不会是最后一个点，并且是链表的合法节点
 * 4、函数中不需要返回任何项
 *
 * 题目保证链表中节点的值互不相同
 *
 */
public class 删除链表节点 {

//    题目：给定一个链表和一个节点指针，在O（1）时间删除该节点。
//    方法：用后一个节点数据覆盖要删除的节点，然后删除下一个节点。
    public static void deleteNode(ListNode node) {
        node.val  = node.next.val;
        node.next = node.next.next;
    }


    public static void main(String[] args) {
        ListNode n1 = new ListNode(3);
        ListNode n2 = new ListNode(5);
        ListNode n3 = new ListNode(7);
        ListNode n4 = new ListNode(2);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        // 删除节点 n2
        deleteNode(n2);
        printList(n1); // 3->7->2
    }
    public static void printList(ListNode node) {
        if (node == null) return;
        if (node.next != null) {
            System.out.print(node.val + "->");
        } else {
            System.out.println(node.val);
        }
        printList(node.next);
    }

}
