package 数据结构专栏.B_链表专栏;

/**
 * @Title 合并两个已排序的链表并将其作为新列表返回。 新链表应该通过拼接前两个链表的节点来完成。
 * @Author zhengqiang.tan
 * @Date 2021/4/17 9:19 PM
 */
public class 合并两个已排序的链表 {

    public static void main(String[] args) {
        //
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
        } else if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }

    }


}
