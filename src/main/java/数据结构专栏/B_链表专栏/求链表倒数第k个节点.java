package 数据结构专栏.B_链表专栏;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 2021/4/17 5:54 PM
 * <p>
 * <p>
 * 题目：输入一个单向链表，输出该链表中倒数第k个节点。
 * 分析: 双指针法，设置p1,p2两个指针，首先p1移动k-1个点，然后p2和p1再一起移动，直到p1到达链表尾部，p2刚好是链表的倒数第k个节点。
 *
 *
 * 给定一个单向链表，要求找出该链表中倒数第 k 个节点，要求只能遍历一次链表，且空间复杂度为 O(1)。
 */
public class 求链表倒数第k个节点 {
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = null;
        ListNode node = FindKthToTail(node1, 3);
        if (node != null)
            System.out.println(node.val);


    }

    public static ListNode FindKthToTail(ListNode head, int k) {
        if (head == null || k <= 0) {
            return null;
        }
        //定义两个指针，一个前指针(p1)和一个后指针(p2)
        ListNode p1 = head;
        ListNode p2 = head;

        //p1先前走k-1步
        for (int i = 0; i < k - 1; i++) {
            //表示k大于链表长度，返回空
            if (p1 == null) {
                return null;
            }
            p1 = p1.next;
        }
        if (p1 == null) {
            return null;
        }
        //快慢指针同时往后遍历
        while (p1.next != null) {
            p1 = p1.next;
            p2 = p2.next;
        }
        return p2;
    }

    public static class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }

}
