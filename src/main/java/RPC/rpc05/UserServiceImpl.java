package RPC.rpc05;

import RPC.common.IUserService;
import RPC.common.User;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:28 PM
 */
public class UserServiceImpl implements IUserService {
    @Override
    public User getUserById(Integer id) {
        return new User(id, "张三");
    }
}
