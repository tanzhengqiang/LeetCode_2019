package RPC.rpc06;

import RPC.common.IProductService;
import RPC.common.Product;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 11:52 PM
 */
public class ProductServiceImpl implements IProductService {
    @Override
    public Product getProductById(Integer id) {
        return new Product(id, "手机");
    }
}
