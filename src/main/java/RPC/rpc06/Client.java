package RPC.rpc06;


import RPC.common.IProductService;
import RPC.common.IUserService;

import java.io.IOException;

/**
 * @Title 版本五：动态代理模式 帮我生成更多类型的代码
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:27 PM
 */
public class Client {
    public static void main(String[] args) {
        IUserService service = (IUserService) Stub.getStub(IUserService.class); //代理产生的新对象
        System.out.println(service.getUserById(123)); // 网络细节被屏蔽，客户端直接调用即可


        IProductService service2 = (IProductService) Stub.getStub(IProductService.class);
        System.out.println(service2.getProductById(234));


    }
}

//net.sf.cglib.proxy.Proxy$ProxyImpl$$EnhancerByCGLIB$$36b30020
//interface RPC.common.IUserService
//        User(id=123, name=张三)
//        net.sf.cglib.proxy.Proxy$ProxyImpl$$EnhancerByCGLIB$$e855024a
//interface RPC.common.IProductService
//        Product(id=234, name=手机)