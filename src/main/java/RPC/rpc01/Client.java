package RPC.rpc01;

import RPC.common.User;

import java.io.*;
import java.net.Socket;

/**
 * @Title 最原始的RPC实现方式
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:27 PM
 */
public class Client {
    public static void main(String[] args) throws IOException {

        Socket s = new Socket("127.0.0.1", 8888);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeInt(11);


        s.getOutputStream().write(baos.toByteArray());
        s.getOutputStream().flush();


        DataInputStream dis = new DataInputStream(s.getInputStream());
        int id = dis.readInt();

        String name = dis.readUTF();
        User user = new User(id, name);

        System.out.println(user);

        dos.close();
        s.close();


    }
}

// output : User(id=11, name=张三)