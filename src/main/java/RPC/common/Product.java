package RPC.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Title
 * @Author zheng.tan
 * @Date 11/14/22 11:51 PM
 */
@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Product implements Serializable {
    private Integer id;
    private String name;
}
