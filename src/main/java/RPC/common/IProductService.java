package RPC.common;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 11:51 PM
 */
public interface IProductService {
    Product getProductById(Integer id);
}
