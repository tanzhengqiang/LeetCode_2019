package RPC.common;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:25 PM
 */
public interface IUserService {
    User getUserById(Integer id);

}