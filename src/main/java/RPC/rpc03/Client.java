package RPC.rpc03;


import RPC.common.IUserService;

import java.io.IOException;

/**
 * @Title 版本三：动态代理模式 缺点：stub里面写死的动态代理，不够灵活
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:27 PM
 */
public class Client {
    public static void main(String[] args) throws IOException {
        IUserService service = Stub.getStub(); //代理产生的新对象
        System.out.println(service.getUserById(123)); // 网络细节被屏蔽，客户端直接调用即可

    }
}
