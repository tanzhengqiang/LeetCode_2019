package RPC.rpc02;

import RPC.common.User;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:27 PM
 */
public class Server {
    private static boolean running = true;

    public static void main(String[] args) throws Exception {
        ServerSocket socket = new ServerSocket(8888);
        while (running) {
            Socket accept = socket.accept();
            process(accept);
            accept.close();
        }
        socket.close();


    }

    private static void process(Socket s) throws Exception {
        InputStream in = s.getInputStream();
        OutputStream out = s.getOutputStream();
        DataInputStream dis = new DataInputStream(in);
        DataOutputStream dos = new DataOutputStream(out);

        int id = dis.readInt();
        UserServiceImpl userService = new UserServiceImpl();
        User user = userService.getUserById(id);

        dos.writeInt(user.getId());
        dos.writeUTF(user.getName());

        dos.flush();

    }
}
