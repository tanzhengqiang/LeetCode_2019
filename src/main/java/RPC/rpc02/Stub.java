package RPC.rpc02;

import RPC.common.User;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * @Title
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:44 PM
 */
public class Stub {

    public static User getUserById(Integer id) throws IOException {

        Socket s = new Socket("127.0.0.1", 8888);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeInt(id);


        s.getOutputStream().write(baos.toByteArray());
        s.getOutputStream().flush();


        DataInputStream dis = new DataInputStream(s.getInputStream());
        int receivedId = dis.readInt();

        String name = dis.readUTF();
        User user = new User(id, name);


        dos.close();
        s.close();

        return user;

    }
}
