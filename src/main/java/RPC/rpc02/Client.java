package RPC.rpc02;


import java.io.IOException;

/**
 * @Title 版本二：把网络传输的部分给我隐藏掉,找个代理
 * @Author zhengqiang.tan
 * @Date 11/14/22 10:27 PM
 */
public class Client {
    public static void main(String[] args) throws IOException {
        Stub stub = new Stub();
        stub.getUserById(123);

    }
}

// output : User(id=11, name=张三)