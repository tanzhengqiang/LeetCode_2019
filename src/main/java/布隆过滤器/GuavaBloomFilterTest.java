package 布隆过滤器;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

/**
 * bloomfilter最核心的功能：判断一定不在某个集合中,但是对于可能出现的有一定的误判率
 */
public class GuavaBloomFilterTest {
    private static int size = 10000;
    private static BloomFilter<Integer> bloomFilter = BloomFilter.create(Funnels.integerFunnel(), size);

    public static void main(String[] args) {
        for (int i = 0; i < size; i++) {
            bloomFilter.put(i);
        }
        System.out.println(bloomFilter.mightContain(356));
        System.out.println(bloomFilter.mightContain(10100));

    }
}
