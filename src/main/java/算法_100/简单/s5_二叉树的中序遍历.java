package 算法_100.简单;


import base.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 二叉树 根 左 右
 * @Created by zhengqiang
 * @Date 2023/1/7 14:18
 */
public class s5_二叉树的中序遍历 {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        dfs(res, root);
        return res;
    }

    void dfs(List<Integer> res, TreeNode root) {
        if (root == null) return;
        dfs(res, root.left);
        res.add(root.val);
        dfs(res, root.right);
    }
}
