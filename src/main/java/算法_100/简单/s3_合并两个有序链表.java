package 算法_100.简单;


import base.ListNode;

/**
 * @Description TODO
 * @Created by zhengqiang
 * @Date 2023/1/7 14:17
 */
public class s3_合并两个有序链表 {

    public static void main(String[] a) {
        System.out.println("hi");

    }

    public ListNode merge(ListNode n1, ListNode n2) {
        if (n1 == null) {
            return n2;
        } else if (n2 == null) {
            return n1;
        } else if (n1.val < n2.val) {
            n1.next = merge(n1, n2.next);
            return n1;
        } else {
            n2.next = merge(n2, n1.next);
            return n2;
        }

    }
}
